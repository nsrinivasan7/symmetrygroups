/*
 * Expressions.h
 *
 *  Created on: May 11, 2017
 *      Author: nsrinivasan7
 */


#include <gtsam/nonlinear/expressions.h>
#include <gtsam/slam/expressions.h>

#include "Space3.h"
#include "Space2.h"
#include "Space1.h"

#include "UnitCell.h"

namespace gtsam {

typedef Pose3_ UnitCell3_;
typedef Pose2_ UnitCell2_;
typedef Double_ UnitCell1_;

typedef Expression<Lattice<1> > Lattice1_;
typedef Expression<Lattice<2> > Lattice2_;

//typedef Expression<Space3<2> > Space32_;
//typedef Expression<Space3<1> > Space31_;
//
//typedef Expression<Space2<2> > Space22_;
//typedef Expression<Space2<1> > Space21_;

typedef Expression<Space11> Space11_;
}
