/*
 * Lattice.h
 *
 *  Created on: Apr 20, 2017
 *      Author: nsrinivasan7
 */

#ifndef LATTICE_H_
#define LATTICE_H_

#include <gtsam/base/Matrix.h>
#include <gtsam/base/VectorSpace.h>

#include <boost/optional.hpp>

/// A lattice class templated on dimension K
namespace gtsam {

template<int K>
class Lattice {
public:

  // is this an over-parameterization ?
  // In 2D for instance we can choose the x-axis as one of the basis, and then
  // we are need to only specify the magnitude of the vector along the x-axis
  // and then an angle and another magnitude so a total of 3 quantities.
  // Similarly in 3D its 5 dimensional (3 magnitudes and 2 angles)

  typedef Eigen::Matrix<double, K, K> Basis;
  typedef Eigen::Matrix<double, K, 1> BasisVectors;
  typedef Eigen::Matrix<double, K * (K + 1) / 2, 1> VectorizedBasis;

  enum {
    dimension = K * (K + 1) / 2
  };

private:
  Basis basis_;

public:
  /// Create basis from vector
  Basis BasisOfVector(const VectorizedBasis& v) const {
    Basis value;
    value.setZero();
    size_t k = 0;
    for(int i = 0; i < K; i++)
      for(int j = 0; j <= i; j++){
        value(j,i) = v(k);
        k++;
    }
    return value;
  }


  Lattice(const Basis& B) {
    // convert to upper triangular matrix to maintain uniform representation
    if (K >= 2) {
      std::pair < Matrix, Matrix > v = gtsam::qr(Matrix(B));
      basis_.block(0, 0, K, K) = v.second;
    } else
      basis_.block(0, 0, K, K) = B;
  }

  /// constructor specified by the column vectors for common cases
  Lattice(const boost::optional<BasisVectors> b1 = boost::none,
      const boost::optional<BasisVectors> b2 = boost::none,
      const boost::optional<BasisVectors> b3 = boost::none) {

    Basis b;
    if (K == 1 && !b2)
      assert("This configuration can have only 1 Basis Vector");

    if (K == 2 && !b3)
      assert("This configuration can have only 2 Basis Vectors");

    if(!b1 && !b2 && !b3){
      b = Basis::Zero();
      goto create;
    }

    if (K >= 1)
      b.block(0, 0, K, 1) = *b1;

    if (K >= 2)
      b.block(0, 1, K, 1) = *b2;

    if (K == 3)
      b.block(0, 2, K, 1) = *b3;

    create:
    new (this) Lattice<K>(b); // TODO: need <K> ??
  }

  /// copy constructor
  Lattice(const Lattice& lattice) :
      basis_(lattice.basis()) {
  }

  //  Lattice(const gtsam::Vector& l) {
  //
  //    int dimension = K*(K+1)/2;
  //    assert(l.rows() != dimension);
  //
  //  }

  const Basis& basis() const {
    return basis_;
  }

  // Vectorspace prerequisites
  //  p = Class::identity();  // identity
  static Lattice identity() {
    return Lattice(Basis::Zero());
  }

  //  q = p + p;              // addition
  Lattice operator+(const Lattice& other) const {
    return Lattice(basis() + other.basis());
  }

  //  q = p + v;              // addition of a vector on the right
  Lattice operator+(const VectorizedBasis& v) const {
    return Lattice(basis() + BasisOfVector(v));
  }

  //  v = p.vector();         // conversion to vector
  VectorizedBasis vector() const {
    VectorizedBasis val; // TODO: implement this
    size_t k = 0;
    for(int i = 0; i < K; i++)
      for(int j = 0; j <= i; j++){
        val(k) = basis_(j,i);
        k++;
    }
    return val;
  }

  //  q = p - p;              // subtraction
  Lattice operator-(const Lattice& other) const {
    return Lattice(basis() - other.basis());
  }

  /// The local coordinates function
//  VectorizedBasis localCoordinates(const Lattice<K>& s) const;

  /// Dimensionality of tangent space = 3 DOF
  inline size_t dim() const {
    return (size_t) dimension;
  }

//  /// The retract function
//  Lattice<K> retract(const VectorizedBasis& v) const;
//
  /// The print function
  void print(const std::string& s = std::string()) const {
    std::cout << s << ": \n" << basis() << std::endl;
  }

  /// The equals function with tolerance
  bool equals(const Lattice<K>& s, double tol = 1e-9) const {
    return equal_with_abs_tol(basis_, s.basis(), tol);
  }
//
//  VectorizedBasis error(const Lattice<K>::VectorizedBasis& v) const;
//
//  VectorizedBasis errorVector(const Lattice<K>& other, OptionalJacobian<K*(K+1)/2, K*(K+1)/2> H1 = boost::none, //
//                      OptionalJacobian<K*(K+1)/2, K*(K+1)/2> H2 = boost::none) const;
//

};

// Some convenient typedefs
typedef Lattice<1> Lattice1;
typedef Lattice<2> Lattice2;
typedef Lattice<3> Lattice3;

template<int K>
struct traits<Lattice<K> > : public internal::VectorSpace<Lattice<K> > {
};

} // gtsam namespace

#endif /* LATTICE_H_ */
