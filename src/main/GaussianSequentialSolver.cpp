#include "stdio.h"

#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/linear/GaussianDensity.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/JunctionTree.h>
#include <gtsam/base/TestableAssertions.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/linear/GaussianBayesNet.h>

/*
Testing a set of linear equations
Ax = b;
3x + 4y = 11
3y + 5x = 16
4y + 6x = 31
*/

using namespace std;
using namespace gtsam;

int main() {
 printf("Hello world /n");
}