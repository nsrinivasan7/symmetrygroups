# If necessary, use the RELATIVE flag, otherwise each source file may be listed 
# with full pathname. RELATIVE may makes it easier to extract an executable name
# automatically.
# file( GLOB APP_SOURCES RELATIVE app/*.cxx )


set(MAIN_LIBS
    symmetrygroups
    ${Boost_LIBRARIES}
    gtsam
    CppUnitLite)

file( GLOB APP_SOURCES "*.cpp" )
#message("${PCL_LIBRARIES}")
foreach( testsourcefile ${APP_SOURCES} )
    # I used a simple string replace, to cut off .cpp.
    get_filename_component(executable "${testsourcefile}" NAME_WE)
    
    add_executable( ${executable} ${testsourcefile} )
    # Make sure YourLib is linked to each app
    target_link_libraries( ${executable} ${MAIN_LIBS})
endforeach( testsourcefile ${APP_SOURCES} )