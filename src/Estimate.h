/*
 * Estiamte.h
 *
 *  Created on: Oct 1, 2017
 *      Author: nsrinivasan7
 */

#ifndef ESTIMATE_H_
#define ESTIMATE_H_

#include <vector>

#include <boost/shared_ptr.hpp>

#include "Space1.h"
#include "MillerIndices.h"


// TODO Never using in a header !!!!!!!
using namespace std;
using namespace gtsam;

enum OptimizeFlag {spaceonly, spaceandpoints, pointsonly, pointandlatticeonly, latticeonly, unitcellonly};
enum Optimizer {LM, GN};

class Estimate1DLattice{
public:
  typedef vector<double> Measurements;
  typedef vector<size_t> Correspondances; // the corresponding index of the primary point
  typedef vector<MillerIndices<1> > SetofMillerIndices; //all the miller indices of the point

private:
  boost::shared_ptr<Measurements> calZ_;
  boost::shared_ptr<Correspondances> mapCalP_;
  boost::shared_ptr<set<size_t> >calP_;
  boost::shared_ptr<SetofMillerIndices> calQ_;
  boost::shared_ptr<vector<double> > calUprime_;
  boost::shared_ptr<Space1<1> > BT_;
  Optimizer optimizerFlag_;
  bool debug_;
public:
  Estimate1DLattice(const Measurements& calZ,const Correspondances& calP,
      const SetofMillerIndices& calQ, const vector<double>& calUprime){
    optimizerFlag_ = Optimizer::LM;
    calZ_.reset(new Measurements(calZ));
    mapCalP_.reset(new Correspondances(calP));
    calQ_.reset(new SetofMillerIndices(calQ));
    calUprime_.reset(new vector<double>(calUprime));
    BT_.reset(new Space1<1>());
    debug_ = true;
  };

  boost::shared_ptr<Measurements> getMeasurements() const {
    return calZ_;
  }

  double getMeasurement(const size_t k) const {
    return calZ_->at(k);
  }

  boost::shared_ptr<Correspondances> getCorrespondance() const {
    return mapCalP_;
  }

  size_t getCorrespondace(const size_t j) const {
    return mapCalP_->at(j);
  }

  void optimizeBT(const double sigma = 0.01){
    optimize(sigma, OptimizeFlag::spaceonly);
  }

  void optimizeBTandPoints(const double sigma = 0.01){
    optimize(sigma, OptimizeFlag::spaceandpoints);
  }

  void optimizePoints(const double sigma = 0.01){
    optimize(sigma, OptimizeFlag::pointsonly);
  }

  void optimizePointsAndLattice(const double sigma = 0.01){
     optimize(sigma, OptimizeFlag::pointandlatticeonly);
   }


  void setDebug(bool value) {
    debug_ = value;
  }

  void getBT(boost::shared_ptr<Space1<1> >& space) {
    space.reset(new Space1<1>(*BT_));
  }

  void SetLatticeAndUnitCell(Space1<1>& space) {
    BT_.reset(new Space1<1>(space));
  }

  void getUprimes(boost::shared_ptr<vector<double> >& calUprime) {
    calUprime.reset(new vector<double>(*calUprime_));
  }

private:
  void optimize(const double sigma, const OptimizeFlag flag);

};

class Estimate1DLattice2DSymmetry{
public:

  typedef vector<Point2> Measurements;
  typedef vector<size_t> Correspondances; // the corresponding index of the primary point
  typedef vector<MillerIndices<1> > SetofMillerIndices; //all the miller indices of the point

private:
  boost::shared_ptr<Measurements> calZ_;
  boost::shared_ptr<Correspondances> f_;
  boost::shared_ptr<set<size_t> >calP_;
  boost::shared_ptr<set<size_t> >calV_;
  boost::shared_ptr<SetofMillerIndices> calQ_;
  boost::shared_ptr<vector<Eigen::Matrix<double, 1, 1> > > calUprime_;
  boost::shared_ptr<Lattice<1> > B_;
  boost::shared_ptr<UnitCell2> T_;
  boost::shared_ptr<vector<double> > calVprime_;
  Optimizer optimizerFlag_;
  bool debug_;
public:

  Estimate1DLattice2DSymmetry(const Measurements& calZ,const Correspondances& calP,
      const SetofMillerIndices& calQ, const vector<Eigen::Matrix<double, 1, 1> >& calUprime,
      const vector<double>& calVprime){
    optimizerFlag_ = Optimizer::LM;
    calZ_.reset(new Measurements(calZ));
    f_.reset(new Correspondances(calP));
    calQ_.reset(new SetofMillerIndices(calQ));
    calUprime_.reset(new vector<Eigen::Matrix<double, 1, 1> >(calUprime));
    calVprime_.reset(new vector<double>(calVprime));
    B_.reset(new Lattice<1>());
    T_.reset(new UnitCell2());
    debug_ = true;
  };

  boost::shared_ptr<Measurements> getMeasurements() const {
    return calZ_;
  }

  Point2 getMeasurement(const size_t k) const {
    return calZ_->at(k);
  }

  boost::shared_ptr<Correspondances> getCorrespondance() const {
    return f_;
  }

  size_t getCorrespondace(const size_t j) const {
    return f_->at(j);
  }

  void optimizeBT(const double sigma = 0.01){
    optimize(sigma, OptimizeFlag::spaceonly);
  }

  void optimizeBTandPoints(const double sigma = 0.01){
    optimize(sigma, OptimizeFlag::spaceandpoints);
  }

  void optimizePoints(const double sigma = 0.01){
    optimize(sigma, OptimizeFlag::pointsonly);
  }

  void optimizePointsAndLattice(const double sigma = 0.01){
     optimize(sigma, OptimizeFlag::pointandlatticeonly);
   }


  void setDebug(bool value) {
    debug_ = value;
  }

  void getBT(boost::shared_ptr<Lattice<1> >& lattice, boost::shared_ptr<UnitCell2>& unitcell) {
    lattice.reset(new Lattice<1>(*B_));
    unitcell.reset(new UnitCell2(*T_));

  }

  void SetLatticeAndUnitCell(Lattice<1>& lattice, UnitCell2& unitcell) {
    T_.reset(new UnitCell2(unitcell));
    B_.reset(new Lattice<1>(lattice));
  }

  void getUprimes(boost::shared_ptr<vector<Eigen::Matrix<double, 1, 1> > >& calUprime) {
    calUprime.reset(new vector<Eigen::Matrix<double, 1, 1> >(*calUprime_));
  }

private:
  void optimize(const double sigma, const OptimizeFlag flag);

};

class Estimate2DLattice2DSymmetry{
public:

  typedef vector<Point2> Measurements;
  typedef vector<size_t> Correspondances; // the corresponding index of the primary point
  typedef vector<MillerIndices<2> > SetofMillerIndices; //all the miller indices of the point

private:
  boost::shared_ptr<Measurements> calZ_;
  boost::shared_ptr<Correspondances> f_;
  boost::shared_ptr<set<size_t> >calP_;
  boost::shared_ptr<SetofMillerIndices> calQ_;
  boost::shared_ptr<vector<Eigen::Matrix<double, 2, 1> > > calUprime_;
  Optimizer optimizerFlag_;
  bool debug_;
public:

  Estimate2DLattice2DSymmetry(const Measurements& calZ,
      const Correspondances& calP, const SetofMillerIndices& calQ,
      const vector<Eigen::Matrix<double, 2, 1> >& calUprime, bool debug = false) :
      debug_(debug) {
    optimizerFlag_ = Optimizer::GN;
    calZ_.reset(new Measurements(calZ));
    f_.reset(new Correspondances(calP));
    calQ_.reset(new SetofMillerIndices(calQ));
    calUprime_.reset(new vector<Eigen::Matrix<double, 2, 1> >(calUprime));
  }

  boost::shared_ptr<Measurements> getMeasurements() const {
    return calZ_;
  }

  Point2 getMeasurement(const size_t k) const {
    return calZ_->at(k);
  }

  boost::shared_ptr<Correspondances> getCorrespondance() const {
    return f_;
  }

  size_t getCorrespondace(const size_t j) const {
    return f_->at(j);
  }

  Values optimizeBT(const double sigma = 0.01){
    return optimize(sigma, OptimizeFlag::spaceonly);
  }

  Values optimizeBTandPoints(const double sigma = 0.01){
    return optimize(sigma, OptimizeFlag::spaceandpoints);
  }

  Values optimizePoints(const double sigma = 0.01){
    return optimize(sigma, OptimizeFlag::pointsonly);
  }

  Values optimizePointsAndLattice(const double sigma = 0.01){
    return optimize(sigma, OptimizeFlag::pointandlatticeonly);
   }


  void setDebug(bool value) {
    debug_ = value;
  }

  void getUprimes(boost::shared_ptr<vector<Eigen::Matrix<double, 2, 1> > >& calUprime) {
    calUprime.reset(new vector<Eigen::Matrix<double, 2, 1> >(*calUprime_));
  }

  // Create factor graph for various scenarios
  NonlinearFactorGraph createGraph(const double sigma, const OptimizeFlag flag) const;

  // Create a good initial estimate
  Values initialEstimate(const OptimizeFlag flag) const;

 private:
  Values optimize(const double sigma, const OptimizeFlag flag) const;

};

#endif /* ESTIMATE_H_ */
