/*
 * Predict.h
 *
 *  Created on: Apr 25, 2017
 *      Author: nsrinivasan7
 *      Description : Predict the location of a point in various scenario
 */

#ifndef PREDICT_H_
#define PREDICT_H_

#include <gtsam/slam/expressions.h>
#include <gtsam/geometry/SimpleCamera.h>
#include <gtsam/nonlinear/ExpressionFactorGraph.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/linear/NoiseModel.h>
#include <gtsam/inference/Symbol.h>

#include "MillerIndices.h"
#include "Space3.h"
#include "Space1.h"


using namespace gtsam;

// given a point in a lattice, we want to be able to predict the 3d Location
// of the point in the Euclidean 3D world.
// predict the location of a point in a lattice given the unit cell and
// Miller indices



template <int N>
struct PredictPointInLattice {

  typedef Eigen::Matrix<double,N,1> PointN;
  MillerIndices<N> q_;

  PredictPointInLattice(const MillerIndices<N>& q): q_(q) {}

  PointN operator()(const PointN& uprime, const Lattice<N>& lattice,
      OptionalJacobian<N,N> H1 = boost::none, OptionalJacobian<N,N*(N+1)/2> H2 = boost::none) {

    PointN u = uprime + q_.indices().template cast<double>();
    PointN translated = lattice.basis() * u;

    if(H1) *H1 = lattice.basis();
    if(H2) {
      Eigen::Matrix<double,N,N*(N+1)/2> val;
      if(N == 1) {
        val << u;
        *H2 = val;
      }
      if(H2 && N == 2) {
        val << u(0),u(1),0,
            0, 0, u(1);
        *H2 = val;
      }

    }


    if(H2 && N >= 3)
      assert("Optional Jacobian Not implemented for N >=3 \n");

    return translated;
  }
};

// define an 3-dimensional point in a K-dimensional lattice
template <int K>
struct PredictPointInSpace3 {

  typedef Eigen::Matrix<double,K,1> PointK;
  typedef Eigen::Matrix<double,3,1> Point3n;

  MillerIndices<K> q_;
  Eigen::Matrix<double,3-K,1> z_;


  PredictPointInSpace3(MillerIndices<K>& q, Eigen::Matrix<double,3-K,1>& z): q_(q),z_(z) {}


  // to compute the jacobian:
  // x = R[y|z] + t
  // for a 1D lattice in 3D space
  // x = |r11 r12 r13|    |y1|    |t1|
  //     |r21 r22 r23|  * |z1|  + |t2|
  //     |r31 r32 r33|    |z2|    |t3|

  // x = |r11 r12 r13|    |b11 *(uprime_1 + q1)|    |t1|
  //     |r21 r22 r23|  * |z1                  |  + |t2|
  //     |r31 r32 r33|    |z2                  |    |t3|

  // lattice here has 1 free parameters and unit cell has 6 free parameters
  // dx/duprime =
  // |b11*r11|
  // |b11*r21|
  // |b11*r31|

  // dx/dspace = 3x7 matrix
  // |uprime_1 * r11 |            |
  // |uprime_1 * r21 | 3x6 matrix |
  // |uprime_1 * r31 |            |

  //*****************************************************************************
  // for a 2D lattice in 3D space
  // x = |r11 r12 r13|    |y1|    |t1|
  //     |r21 r22 r23|  * |y1|  + |t2|
  //     |r31 r32 r33|    |z2|    |t3|

  // x = |r11 r12 r13|    |b11 *(uprime_1 + q1) + b12*(uprime_2 + q2)|    |t1|
  //     |r21 r22 r23|  * |b22 *(uprime_2 + q2)                      |  + |t2|
  //     |r31 r32 r33|    |z2                                        |    |t3|

  // lattice here has 1 free parameters and unit cell has 6 free parameters
  // dx/duprime =
  // |b11*r11|
  // |b11*r21|
  // |b11*r31|

  // dx/dspace = 3x7 matrix
  // |uprime_1 * r11 |            |
  // |uprime_1 * r21 | 3x6 matrix |
  // |uprime_1 * r31 |            |


  Point3 operator()(const PointK& uprime, Space3<K>& space,
      OptionalJacobian<3,K> H1 = boost::none, OptionalJacobian<3,K*(K+1)/2+6> H2 = boost::none) {

    PredictPointInLattice<K> predictpointinlattice(q_);

    Eigen::Matrix<double,K,K> val4;
    Eigen::Matrix<double,K,K*(K+1)/2> val5;

    PointK y = predictpointinlattice(uprime,space.lattice(), val4, val5);
    Point3 x = space.getxfromy(y,z_);


    Matrix33 R = space.unitcell().rotation().matrix();
    const Lattice<K>& lattice = space.lattice();
    const UnitCell3& unitcell = space.unitcell();

    if(H1) {
      (*H1).block(0,0,K,K) << R.block(0,0, K, K) * lattice.basis();
      (*H1).block(K,0,3-K,K) << 0.0, 0.0;
    }

    if(H2) {
      (*H2).block(0,0,K,K*(K+1)/2) << val5; // the lattice derivative
      (*H2).block(K,0,3-K,K*(K+1)/2).setZero();
      Matrix36 val2;
      Matrix33 val3;
      Vector3 p_vector;p_vector << y,z_;
      Point3 p(p_vector);
      (void)unitcell.transform_from(p,val2,val3);
      (*H2).block(0,K*(K+1)/2,3,6) << val2;
    }

    return x;
  }
};

// define an 2-dimensional point in a K-dimensional lattice
struct PredictPointInSpace21 {

  MillerIndices<1> q_;
  double vprime_;


  PredictPointInSpace21(MillerIndices<1>& q, double vprime ): q_(q),vprime_(vprime) {}

  PredictPointInSpace21(MillerIndices<1>& q): q_(q), vprime_(0.0) {}

  Point2 operator()(const double& uprime,const Lattice<1>& lattice,const UnitCell2& pose,
      OptionalJacobian<2,1> H1 = boost::none, OptionalJacobian<2,1> H2 = boost::none,
      OptionalJacobian<2,3> H3 = boost::none) {

    PredictPointInLattice<1> predictpointinlattice(q_);

    Matrix11 val4;
    Matrix11 val5;
    Vector1 uprime_vector;
    uprime_vector << uprime;

    Vector bu = predictpointinlattice(uprime_vector,lattice, val4, val5);


    Matrix23 val2;
    Matrix22 val3;
    Vector2 v; v << val5, 0.0;
    Point2 x = pose.transform_from(Point2(bu(0), vprime_), val2, val3);

    if(H1) {
      (*H1).block(0,0,2,1) << val3.block(0,0,2,1);
    }

    if(H2) {
      (*H2) << val3 * v; // the lattice derivative
    }

    if(H3) {
      (*H3) << val2;
    }

    return x;
  }
};

struct transform_from_Pose2 {

  Point2 operator()(const Point2& point, const UnitCell2& pose,
      OptionalJacobian<2,2> H1 = boost::none, OptionalJacobian<2,3> H2 = boost::none) {
    return pose.transform_from(point, H2, H1);
  }
};

// define an 2-dimensional point in a K-dimensional lattice
struct PredictPointInSpace21GivenUnitCell {

  MillerIndices<1> q_;

  PredictPointInSpace21GivenUnitCell(MillerIndices<1>& q): q_(q) {}

  Point2 operator()(const double& uprime, const double& vprime, const Lattice<1>& lattice,
      OptionalJacobian<2,1> H1 = boost::none, OptionalJacobian<2,1> H2 = boost::none,
      OptionalJacobian<2,1> H3 = boost::none) {

    PredictPointInLattice<1> predictpointinlattice(q_);

    Matrix11 val4;
    Matrix11 val5;
    Vector1 uprime_vector;
    uprime_vector << uprime;

    Vector bu = predictpointinlattice(uprime_vector,lattice, val4, val5);


    if(H1) {
      (*H1) << val4, 0.0;
    }

    if(H2) {
      (*H2) << 0.0, 1.0;
    }

    if(H3) {
      (*H3) << val5, 0.0;
    }

    return Point2(bu(0), vprime);
  }
};


// define an 2-dimensional point in a K-dimensional lattice
struct PredictPointInSpace22 {

  MillerIndices<2> q_;

  PredictPointInSpace22(MillerIndices<2>& q): q_(q) {}

  Point2 operator()(const Point2& uprime,const Lattice<2>& lattice,const UnitCell2& pose,
      OptionalJacobian<2,2> H1 = boost::none, OptionalJacobian<2,3> H2 = boost::none,
      OptionalJacobian<2,3> H3 = boost::none) {

    PredictPointInLattice<2> predictpointinlattice(q_);

    Matrix val4;
    Matrix val5;

    Eigen::Matrix<double,2,1> p; p << uprime.x(), uprime.y();
    Vector bu = predictpointinlattice(p,lattice, val4, val5);


    Matrix23 val2;
    Matrix22 val3;
    Point2 x = pose.transform_from(Point2(bu(0), bu(1)), val2, val3);

    if(H1) {
      (*H1) << val3* val4;
    }

    if(H2) {
      (*H2) << val3 * val5; // the lattice derivative
    }

    if(H3) {
      (*H3) << val2;
    }

    return x;
  }
};

// define an 1-dimensional point in a 1-dimensional lattice
struct PredictPointInSpace1 {

  MillerIndices<1> q_;
  UnitCell1 pose_;
  PredictPointInSpace1(MillerIndices<1>& q): q_(q), pose_(0.0) {}

  double operator()(const double& uprime, const Space1<1>& space,
      OptionalJacobian<1,1> H1 = boost::none, OptionalJacobian<1,2> H2 = boost::none) {

    PredictPointInLattice<1> predictpointinlattice(q_);

    Eigen::Matrix<double,1,1> val4;
    Eigen::Matrix<double,1,1> val5;
    Eigen::Matrix<double,1,1> uprime_as_matrix; uprime_as_matrix << uprime;

    Eigen::Matrix<double,1,1> y_as_matrix = predictpointinlattice(uprime_as_matrix,space.lattice(), val4, val5);
    double x = space.getxfromy(y_as_matrix);


    const Lattice<1>& lattice = space.lattice();

    if(H1) {
      (*H1).block(0,0,1,1) << lattice.basis()(0);
    }

    if(H2) {
      (*H2).block(0,0,1,1) << val5(0);
      (*H2).block(0,1,1,1) << 1.0; // dericatiove qrt to unit cell
    }
    return x;
  }
};


struct PredictPointInSpaceGivenUnitCell1 {
  MillerIndices<1> q_;
  UnitCell1 pose_;
  PredictPointInSpaceGivenUnitCell1(MillerIndices<1>& q, const UnitCell1& pose): q_(q), pose_(pose) {}

  double operator()(const double& uprime, const Lattice1& lattice,
      OptionalJacobian<1,1> H1 = boost::none, OptionalJacobian<1,1> H2 = boost::none) {

    PredictPointInLattice<1> predictpointinlattice(q_);

    Eigen::Matrix<double,1,1> val4;
    Eigen::Matrix<double,1,1> val5;
    Eigen::Matrix<double,1,1> uprime_as_matrix; uprime_as_matrix << uprime;

    Eigen::Matrix<double,1,1> y_as_matrix = predictpointinlattice(uprime_as_matrix,lattice, val4, val5);
    Space11 space(lattice, pose_);
    double x = space.getxfromy(y_as_matrix);

    if(H1) {
      (*H1).block(0,0,1,1) << lattice.basis()(0);
    }

    if(H2) {
      (*H2).block(0,0,1,1) << val5(0);
    }
    return x;
  }
};

//struct Predict1DRot {
  //  int u_;
//  Predict1DRot(int u) :
//    u_(u) {
//  }
//
//  gtsam::Point3 operator()(const gtsam::Point3& p, const gtsam::Rot3& g, //
//      gtsam::OptionalJacobian<3, 3> H1, gtsam::OptionalJacobian<3, 3> H2) {
//
//    gtsam::Vector3 theta_ = gtsam::Rot3::Logmap(g);
//    gtsam::Vector3 utheta_ = u_ * theta_;
//    gtsam::Rot3 Ru = gtsam::Rot3::Expmap( utheta_ );
//
//    if(H1 || H2)
//    {
//      *H1 << Ru.matrix();
//      Matrix3 H2_temp;
//      H2_temp.setZero();
//      for(int i = 0; i < (u_-1) ; i++)
//      {
//        gtsam::Matrix3 R_u_i = ( gtsam::Rot3::Expmap((u_-i) * theta_) ).matrix();
//        gtsam::Matrix3 R_i = ( gtsam::Rot3::Expmap(i * theta_) ).matrix();
//        H2_temp = H2_temp - R_u_i * skewSymmetric(R_i * p.vector());
//      }
//      *H2 << H2_temp;
//    }
//    // if(H1 || H2)
//    // {
//    //   *H1 << Ru.matrix();
//    //   Matrix3 H2_temp;
//    //   H2_temp.setZero();
//    //   for(int i = 1; i <= u_ ; i++)
//    //   {
//    //     gtsam::Matrix3 R_u_i = ( gtsam::Rot3::Expmap((u_-i) * theta_) ).matrix();
//    //     gtsam::Matrix3 R_i = ( gtsam::Rot3::Expmap(i * theta_) ).matrix();
//    //     H2_temp = H2_temp - R_i * skewSymmetric(R_u_i * p.vector());
//    //   }
//    //   *H2 << H2_temp;
//    // }
//    return Ru.rotate(p);
//  }
//};
//
//struct Predict1DAngle {
//  int u_;
//  Unit3 axis_;
//  Predict1DAngle(int u, Unit3 axis) :
//    u_(u),axis_(axis) {
//  }
//
//  gtsam::Point3 operator()(const gtsam::Point3& p, const double& g, //
//      gtsam::OptionalJacobian<3, 3> H1, gtsam::OptionalJacobian<3, 1> H2) {
//
//    gtsam::Point3 utheta_ = (double)u_ *g * axis_;
//    gtsam::Rot3 Ru = gtsam::Rot3::Expmap( utheta_.vector() );
//    gtsam::Vector3 theta_ = g * axis_.point3().vector();
//
//    if(H1 || H2)
//    {
//      *H1 << Ru.matrix();
//      Matrix3 H2_temp;
//      H2_temp.setZero();
//      for(int i = 0; i < (u_-1) ; i++)
//      {
//        gtsam::Matrix3 R_u_i = ( gtsam::Rot3::Expmap((u_-i) * theta_) ).matrix();
//        gtsam::Matrix3 R_i = ( gtsam::Rot3::Expmap(i * theta_) ).matrix();
//        H2_temp = H2_temp - R_u_i * skewSymmetric(R_i * p.vector());
//      }
//      *H2 << H2_temp * axis_.point3().vector();
//    }
//    // if(H1 || H2)
//    // {
//    //   *H1 << Ru.matrix();
//    //   Matrix3 H2_temp;
//    //   H2_temp.setZero();
//    //   for(int i = 1; i <= u_ ; i++)
//    //   {
//    //     gtsam::Matrix3 R_u_i = ( gtsam::Rot3::Expmap((u_-i) * theta_) ).matrix();
//    //     gtsam::Matrix3 R_i = ( gtsam::Rot3::Expmap(i * theta_) ).matrix();
//    //     H2_temp = H2_temp - R_i * skewSymmetric(R_u_i * p.vector());
//    //   }
//    //   *H2 << H2_temp;
//    // }
//    return Ru.rotate(p);
//  }
//};
//
//// real case when the period has all three axis of translation
//struct Predict1DR {
//  size_t k;
//  Predict1DR(size_t k):k(k) {}
//
//  Point3 operator()(const Point3& p, const Point3& tx,
//      OptionalJacobian<3,3> H1, OptionalJacobian<3,3> H2) {
//    Point3 translated = p + k*tx;
//
//    if(H1) *H1 = gtsam::I_3x3;
//    if(H2) {
//      gtsam::Matrix33 val;
//      val.setIdentity();
//      val = k*val;
//      *H2 = val;
//    }
//    return translated;
//  }
//};
//
//struct TransformRotation {
//  Point3 translation_;
//  TransformRotation(Point3 translation) : translation_(translation) {};
//
//
//  Point3 operator()(const Point3& p, const Rot3& rotation,
//      OptionalJacobian<3,3> H1, OptionalJacobian<3,3> H2) {
//
//    gtsam::Pose3 pose(rotation,translation_);
//
//    gtsam::Matrix33 val1; gtsam::Matrix36 val2;
//    Point3 transformed = pose.transform_to(p,val2,val1);
//
//    if(H1) *H1 = val1;
//    if(H2) {
//      *H2 = val2.block<3,3>(0,0);
//    }
//    return transformed;
//  }
//
//};
//
//class Predict {
//
//  static void Optimize();
//};



#endif /* GENERATEFACTOR_H_ */




