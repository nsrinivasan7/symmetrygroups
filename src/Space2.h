/*
 * Space2.h
 *
 *  Created on: Sep 22, 2017
 *      Author: nsrinivasan7
 */

#ifndef SRC_SPACE2_H_
#define SRC_SPACE2_H_

#include "Lattice.h"
#include "UnitCell.h"

#include <gtsam/geometry/Pose2.h>

namespace gtsam {

// a K-dimensional lattice in a 2-dimensional space
template<int K>
class Space2 {
public:

  typedef Eigen::Matrix<double, K, 1> PointK;
  typedef Eigen::Matrix<double, K * (K + 1) / 2 + 3, 1> VectorizedSpace2;

  enum {
    dimension = (K * (K + 1) / 2) + 3
  };

private:

  Lattice<K> lattice_;
  UnitCell2 unitcell_;

public:
  /// Create basis from vector
  static Space2<K> BTOfVector(const VectorizedSpace2& v) {
    if(K == 1)
      return Space2<K> (Lattice<K>(v.block(0,0,1,1)), Pose2(v(1), v(2), v(3)));

    if(K == 2) {
      Eigen::Matrix<double, K, K> basis;
      basis << v(0), v(1), 0.0 , v(2);
      return Space2<K> (Lattice<K>(basis), Pose2(v(3), v(4), v(5)));
    }

    return Space2<K>();
  }

  Space2(const Lattice<K>& lattice, const UnitCell2& unitcell) :
    lattice_(lattice), unitcell_(unitcell) {
    assert(K > 1);
  }

  Space2() :
    lattice_(Lattice<K>()), unitcell_(Pose2()) {
    assert(K > 1);
  }

  Space2(const VectorizedSpace2 b1) {
      Space2<K> space = BTOfVector(*b1);
      new (this) Space2<K>(space); // TODO: need <K> ??
  }

  const Lattice<K>& lattice() const {
    return lattice_;
  }

  const UnitCell2& unitcell() const{
     return unitcell_;
   }

   Space2(const Space2& space) :
     lattice_(space.lattice()), unitcell_(space.unitcell()) {

   }

   static Space2 identity() {
     return Space2(Lattice<K>(), Pose2());
   }

   //  q = p + p;              // addition
   Space2 operator+(const Space2& other) const {
     return Space2(lattice()+other.lattice(), unitcell()+other.unitcell());
   }

   //  q = p + v;              // addition of a vector on the right
   Space2 operator+(const VectorizedSpace2& v) const {
     Space2<K> val =BTOfVector(v);
     return (Space2(lattice(), unitcell()) + val);
   }

   //  v = p.vector();         // conversion to vector
   VectorizedSpace2 vector() const {
     return VectorizedSpace2(); // TODO: implement this
   }

   //  q = p - p;              // subtraction
   Space2 operator-(const Space2& other) const {
     return Space2(lattice() - other.lattice(), unitcell() - other.unitcell() );
   }

   /// Dimensionality of tangent space = 3 DOF
   inline size_t dim() const {
     return (size_t) dimension;
   }

   /// The print function
   void print(const std::string& s = std::string()) const {
     std::cout << s << "\nBasis = \n " <<lattice_.basis();
     unitcell_.print("Unit Cell \n");
   }

   // The equals function with tolerance
   bool equals(const Space2<K>& s, double tol = 1e-9) const {
     bool val1 =  equal_with_abs_tol(lattice().basis(), s.lattice().basis(), tol);
     bool val2 = s.unitcell().equals(unitcell(), tol);
     return val1 && val2;
   }

  PointK getBu(const PointK& u) {
    return (lattice_.basis() * u);
  }

  Point2 getx(const PointK& u,
      boost::optional<const Eigen::Matrix<double, 2 - K, 1>&> vprime = boost::none) {
    if (vprime && K == 2)
      assert("vprime has to be zero for this setting \n");

    PointK bu = getBu(u);
    if(K == 2)
      return unitcell_.transform_from(Point2(bu(0), bu(1)));

    Point2 w(bu(0), vprime);
    return unitcell_.transform_from(w);
  }

  Point2 getxfromy(const PointK& bu,
      boost::optional<const Eigen::Matrix<double, 2 - K, 1>&> vprime = boost::none) {
    if (vprime && K == 2)
      assert("z has to be zero for this setting \n");

    if(K == 2)
      return unitcell_.transform_from(Point2(bu(0), bu(1)));

    Point2 w(bu(0), vprime);
    return unitcell_.transform_from(w);
  }

};

} //gtsam namespace

#endif /* SRC_SPACE2_H_ */
