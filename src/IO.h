/*
 * IO.h
 *
 *  Created on: Sep 30, 2017
 *      Author: nsrinivasan7
 */

#include <vector>
#include <fstream>
#include <gtsam/geometry/Point3.h>
#include <gtsam/geometry/Point2.h>

using namespace std;
using namespace gtsam;

void create_full_path_to_test_data(const string filename);

void write_values_to_ascii_file(const string filename, const vector<double>& values);

void write_points_to_ascii_file(const string filename, const vector<Point2>& points);



