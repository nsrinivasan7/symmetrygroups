/*
 * MillerIndices.h
 *
 *  Created on: Apr 25, 2017
 *      Author: nsrinivasan7
 */

#ifndef MILLERINDICES_H_
#define MILLERINDICES_H_

#include <gtsam/geometry/Pose3.h>
#include <boost/optional.hpp>

// it is templates on the dimension of the lattice N
template<int N>
class MillerIndices {

public:
  typedef Eigen::Matrix<int, N, 1> Indices;

private:
  Indices indices_;

public:

  const Indices& indices() const {
    return indices_;
  };

  MillerIndices(const boost::optional<int> i = boost::none,
      const boost::optional<int> j=boost::none,
      const boost::optional<int> k=boost::none) {

    if(N ==1 && !j)
      assert("This configuration can have only 1 Miller index");

    if(N ==2 && !k)
      assert("This configuration can have only 2 Miller indices");

    if(N >= 1)
      indices_(0) = *i;

    if(N >= 2)
      indices_(1) = *j;

    if(N == 3)
      indices_(2) = *k;
  }

  MillerIndices(const Indices& indices) :
    indices_(indices){
  };

  /// copy constructor
  MillerIndices(const MillerIndices& millerindices) :
      indices_(millerindices.indices()) {
  }

  int at(const size_t index) const{
    return indices_(index);
  }

  Indices getIndices() const {
    return indices_;
  }
};



#endif /* MILLERINDICES_H_ */
