/*
 * Space1.h
 *
 *  Created on: Sep 22, 2017
 *      Author: nsrinivasan7
 */

#ifndef SPACE1_H_
#define SPACE1_H_

#include <utility>

#include "Lattice.h"
#include "UnitCell.h"

namespace gtsam {

// a K-dimensional lattice in a 2-dimensional space
template<int K>
class Space1 {
public:

  typedef Eigen::Matrix<double, K, 1> PointK;
  typedef Eigen::Matrix<double, K * (K + 1) / 2 + 1, 1> VectorizedSpace1;

  enum {
    dimension = (K * (K + 1) / 2) + 1
  };

private:

  Lattice<K> lattice_;
  UnitCell1 unitcell_;

public:
  /// Create basis from vector
  static Space1<K> BTOfVector(const VectorizedSpace1& v) {
    return Space1<K> (Lattice<K>(v.block(0,0,1,1)), (UnitCell1)v(1));
  }

  Space1() :
    lattice_(Lattice<K>()), unitcell_(0.0) {
    assert(K > 1);
  }

  Space1(const Lattice<K>& lattice, const UnitCell1& pose) :
    lattice_(lattice), unitcell_(pose) {
    assert(K > 1);
  }

  Space1(const VectorizedSpace1& b1) {
    Lattice<K> l(b1->block(0,0,1,1));
    UnitCell1 u = b1(1);

    new (this) Space1<K>(l, u); // TODO: need <K> ??

  }

  const Lattice<K>& lattice() const {
    return lattice_;
  }

  const UnitCell1& unitcell() const {
    return unitcell_;
  }

  Space1(const Space1& space) :
    lattice_(space.lattice()), unitcell_(space.unitcell()) {
  }

  static Space1 identity() {
    return Space1(Lattice<K>(), (UnitCell1)(0.0));
  }

  //  q = p + p;              // addition
  Space1 operator+(const Space1& other) const {
    return Space1(lattice()+other.lattice(), unitcell()+other.unitcell());
  }

  //  q = p + v;              // addition of a vector on the right
  Space1 operator+(const VectorizedSpace1& v) const {
    Space1<K> val =BTOfVector(v);
    return (Space1(lattice(), unitcell()) + val);
  }

  //  v = p.vector();         // conversion to vector
  VectorizedSpace1 vector() const {
    return VectorizedSpace1(); // TODO: implement this
  }

  //  q = p - p;              // subtraction
  Space1 operator-(const Space1& other) const {
    return Space1(lattice() - other.lattice(), unitcell() - other.unitcell() );
  }

  /// Dimensionality of tangent space = 3 DOF
  inline size_t dim() const {
    return (size_t) dimension;
  }

  /// The print function
  void print(const std::string& s = std::string()) const {
    std::cout << s << "\nBasis = \n " <<lattice_.basis() << "\nUnit Cell = \n " << unitcell_ << std::endl;
  }

  // The equals function with tolerance
  bool equals(const Space1<K>& s, double tol = 1e-9) const {
    bool val1 =  equal_with_abs_tol(lattice().basis(), s.lattice().basis(), tol);
    Matrix11 a,b; a << unitcell_; b << s.unitcell();
    bool val2 =  equal_with_abs_tol(a, b, tol);
    return val1 && val2;
  }


  PointK getBu(const PointK& u) {
    return (lattice_.basis() * u);
  }

  double getx(const PointK& u) {
    PointK y = getBu(u);
    return (y(0)+ unitcell_);
  }

  double getxfromy(const PointK& y) const {
    return (y(0) + unitcell_);
  }
};

template<int K>
struct traits<Space1<K> > : public internal::VectorSpace<Space1<K> > {
};

typedef Space1<1> Space11;
} // namespace gtsam


#endif /* SPACE1_H_ */
