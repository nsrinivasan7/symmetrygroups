/*
 * Estiamte.cpp
 *
 *  Created on: Oct 1, 2017
 *      Author: nsrinivasan7
 */
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>

#include "Expressions.h"
#include "Estimate.h"
#include "Predict.h"

using namespace noiseModel;

void Estimate1DLattice::optimize(const double sigma, const OptimizeFlag flag) {
  // factor graph
  ExpressionFactorGraph graph;

  // measurement noise
  Isotropic::shared_ptr measurementNoise =
      Isotropic::Sigma(1, sigma);

  Values initial, result;

  // create the graph
  switch(flag) {
  case spaceonly: {
    Space11_ unknowSpace('s', 0);
    size_t K = getMeasurements()->size();
    for(size_t k=0; k < K; k++) {
      MillerIndices<1> q = calQ_->at(k);
      PredictPointInSpace1 predictPointinSpace(q);
      size_t p = mapCalP_->at(k);
      double z_k = getMeasurement(k);
      double uprime = calUprime_->at(p);
      if(debug_)printf("p, k/K, q, z_k, uprime %lu, %lu/%lu, %d, %lf, %lf \n", p, k, K, q.at(0), z_k, uprime);
      assert(p < calUprime_->size());
      Double_ uprimeEspression(uprime);
      Double_ predictPointinSpaceExpression(predictPointinSpace, uprimeEspression, unknowSpace);
      graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
    }
    initial.insert(Symbol('s',0), Space11(Lattice1(), 0.0));
    break;
  }
  case spaceandpoints: {
    Space11_ unknowSpace('s', 0);
    size_t K = getMeasurements()->size();
    calP_.reset(new set<size_t>());
    for(size_t k=0; k < K; k++) {
      MillerIndices<1> q = calQ_->at(k);
      PredictPointInSpace1 predictPointinSpace(q);
      size_t p = mapCalP_->at(k);
      double z_k = getMeasurement(k);
      double uprime = calUprime_->at(p);
      if(debug_)printf("p, k/K, q, z_k, uprime %lu, %lu/%lu, %d, %lf, %lf \n", p, k, K, q.at(0), z_k, uprime);
      assert(p < calUprime_->size());

      if(p == 0) {
        Double_ knownuprimeEspression(0.0);
        Double_ predictPointinSpaceExpression(predictPointinSpace, knownuprimeEspression, unknowSpace);
        graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
      }else {
        Symbol key('u', p);
        Double_ unknownuprimeEspression(key);
        calP_->insert(key);
        Double_ predictPointinSpaceExpression(predictPointinSpace, unknownuprimeEspression, unknowSpace);
        graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
      }


    }
    initial.insert(Symbol('s',0), Space11(Lattice1(), 0.0));
    set<size_t>::iterator it = calP_->begin();
    for(; it != calP_->end(); it++) {
      initial.insert(*it, 0.0);
    }
    break;
  }
  case pointsonly: {
    Space11_ knownSpace(*BT_);
    size_t K = getMeasurements()->size();
    calP_.reset(new set<size_t>());
    for(size_t k=0; k < K; k++) {
      MillerIndices<1> q = calQ_->at(k);
      PredictPointInSpace1 predictPointinSpace(q);
      size_t p = mapCalP_->at(k);
      double z_k = getMeasurement(k);
      double uprime = calUprime_->at(p);
      if(debug_)printf("p, k/K, q, z_k, uprime %lu, %lu/%lu, %d, %lf, %lf \n", p, k, K, q.at(0), z_k, uprime);
      assert(p < calUprime_->size());
      Symbol key('u', p);
      Double_ unknownuprimeEspression(key);
      calP_->insert(key);
      Double_ predictPointinSpaceExpression(predictPointinSpace, unknownuprimeEspression, knownSpace);
      graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
    }
    for(size_t p =0; p < calUprime_->size(); p++) {
      initial.insert(Symbol('u', p), 0.0);
    }
    break;
  }

  case pointandlatticeonly: {
    Lattice1_ unknownLattice('l', 0);
    size_t K = getMeasurements()->size();
    calP_.reset(new set<size_t>());
    for(size_t k=0; k < K; k++) {
      MillerIndices<1> q = calQ_->at(k);
      PredictPointInSpaceGivenUnitCell1 predictPointinSpace(q, BT_->unitcell());
      size_t p = mapCalP_->at(k);
      double z_k = getMeasurement(k);
      double uprime = calUprime_->at(p);
      if(debug_)printf("p, k/K, q, z_k, uprime %lu, %lu/%lu, %d, %lf, %lf \n", p, k, K, q.at(0), z_k, uprime);
      assert(p < calUprime_->size());
      Symbol key('u', p);
      Double_ unknownuprimeEspression(key);
      calP_->insert(key);
      Double_ predictPointinSpaceExpression(predictPointinSpace, unknownuprimeEspression, unknownLattice);
      graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
    }
    initial.insert(Symbol('l',0), Lattice1());
    for(size_t p =0; p < calUprime_->size(); p++) {
      initial.insert(Symbol('u', p), 0.0);
    }
    break;
  }
  default:
    assert("Optimization Configuration Undefined! \n");
  }

  switch (optimizerFlag_) {
  case LM: {
    if(debug_) {
      LevenbergMarquardtParams params;
      params.setVerbosityLM("TRYDELTA");
      result = LevenbergMarquardtOptimizer(graph, initial,params).optimize();
    } else
      result = LevenbergMarquardtOptimizer(graph, initial).optimize();
    break;
  }
  case GN: {
    result = GaussNewtonOptimizer(graph, initial).optimize();
    break;
  }
  default:
    assert("Optimization Type Undefined! \n");
  }

  switch(flag){
  case spaceonly: {
    BT_.reset(new Space11(result.at<Space11>(Symbol('s',0))));
    break;
  }
  case spaceandpoints: {
    result.print("Result as Values = \n");
    BT_.reset(new Space11(result.at<Space11>(Symbol('s',0))));
    result.at<Space11>(Symbol('s',0)).print("BT(result)= \n");
    set<size_t>::iterator it = calP_->begin();
    calUprime_.reset(new vector<double>());
    calUprime_->push_back(0.0);
    for(; it != calP_->end() ; it++) {
      calUprime_->push_back(result.at<double>(*it));
      printf("result.at<double>(*it) = %lf", result.at<double>(*it));
    }
    break;
  }
  case pointsonly: {
    result.print("Result as Values = \n");
    set<size_t>::iterator it = calP_->begin();
    calUprime_.reset(new vector<double>());
    for(; it != calP_->end() ; it++) {
      calUprime_->push_back(result.at<double>(*it));
      printf("result.at<double>(*it) = %lf", result.at<double>(*it));
    }
    break;
  }
  case pointandlatticeonly: {
    BT_.reset(new Space11(result.at<Lattice1>(Symbol('l',0)), BT_->unitcell()));
    result.print("Result as Values = \n");
    set<size_t>::iterator it = calP_->begin();
    calUprime_.reset(new vector<double>());
    for(; it != calP_->end() ; it++) {
      calUprime_->push_back(result.at<double>(*it));
      printf("result.at<double>(*it) = %lf", result.at<double>(*it));
    }
    break;
  }
  default:
    assert("Optimization Type Undefined! \n");
  }
  if(debug_) {
    cout << "Initial Error = " << graph.error(initial) << endl;
    cout << "Final Error = " << graph.error(result) << endl;
  }
}


void Estimate1DLattice2DSymmetry::optimize(const double sigma,
    const OptimizeFlag flag) {
  // factor graph
  ExpressionFactorGraph graph;

  // measurement noise
  Isotropic::shared_ptr measurementNoise =
      Isotropic::Sigma(2, sigma);

  Values initial, result;

  // create the graph
  switch(flag) {
  case spaceonly: {
    Lattice1_ unknowLattice('l', 0);
    UnitCell2_ unknowunitcell('x', 0);

    size_t K = getMeasurements()->size();
    for(size_t k=0; k < K; k++) {
      MillerIndices<1> q = calQ_->at(k);
      size_t p = f_->at(k);
      double vprime = calVprime_->at(p);
      PredictPointInSpace21 predictPointinSpace(q,vprime );
      Point2 z_k = getMeasurement(k);
      double uprime = calUprime_->at(p)(0);
      Point2 predicted = predictPointinSpace(uprime, *B_, *T_);
      if(debug_)printf("j = %lu, k/K = (%lu/%lu), q= %d, z_k= (%lf, %lf), uprime = (%lf),vprime = (%lf), predicted = (%lf, %lf), B = %lf \n", p, k, K, q.at(0), z_k.x(), z_k.y(), uprime, vprime, predicted.x(), predicted.y(), (*B_).basis()(0));
      assert(p < calUprime_->size());
      Double_ uprimeEspression(uprime);
      Point2_ predictPointinSpaceExpression(predictPointinSpace, uprimeEspression, unknowLattice, unknowunitcell);
      graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
    }
    initial.insert(Symbol('l',0), Lattice<1>());
    initial.insert(Symbol('x',0), Pose2());

    break;
  }
  case spaceandpoints: {
    Lattice1_ unknowLattice('l',0);
    UnitCell2_ unknowunitcell('x',0);

    size_t K = getMeasurements()->size();
    calP_.reset(new set<size_t>());
    calV_.reset(new set<size_t>());

    for(size_t k=0; k < K; k++) {
      MillerIndices<1> q = calQ_->at(k);
      PredictPointInSpace21GivenUnitCell predictPointinSpace(q);
      transform_from_Pose2 transform_from;
      size_t p = f_->at(k);
      Point2 z_k = getMeasurement(k);
      double uprime = calUprime_->at(p)(0);
      double vprime = calVprime_->at(p);
      Point2 predicted = transform_from(predictPointinSpace(uprime, vprime, *B_), *T_);
      if(debug_)printf("j = %lu, k/K = (%lu/%lu), q= %d, z_k= (%lf, %lf), uprime = (%lf),vprime = (%lf), predicted = (%lf, %lf), B = %lf \n", p, k, K, q.at(0), z_k.x(), z_k.y(), uprime, vprime, predicted.x(), predicted.y(), (*B_).basis()(0));
      assert(p < calUprime_->size());


      if(p == 0) {
        Double_ knownuprimeExpression(0.0);
        Double_ knownvprimeExpression(0.0);
        Point2_ predictPointinSpaceExpressionGivenUnitCell(predictPointinSpace, knownuprimeExpression, knownvprimeExpression, unknowLattice);
        Point2_ predictPointinSpaceExpression(transform_from, predictPointinSpaceExpressionGivenUnitCell, unknowunitcell);
        graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
      }else {
        Symbol keyu('u', p);
        Symbol keyv('v', p);
        Double_ unknownuprimeExpression(keyu);
        Double_ unknownvprimeExpression(keyv);
        calP_->insert(keyu);
        calV_->insert(keyv);
        Point2_ predictPointinSpaceExpressionGivenUnitCell(predictPointinSpace, unknownuprimeExpression, unknownvprimeExpression, unknowLattice);
        Point2_ predictPointinSpaceExpression(transform_from, predictPointinSpaceExpressionGivenUnitCell, unknowunitcell);
        graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
      }


    }
    initial.insert(Symbol('l',0), Lattice<1>());
    initial.insert(Symbol('x',0), UnitCell2());
    set<size_t>::iterator it;
    it = calP_->begin();
    for(; it != calP_->end(); it++)
      initial.insert(*it, 0.0);

    for(it = calV_->begin(); it != calV_->end(); it++)
      initial.insert(*it, 0.0);

    graph.print("Graph : \n");

    break;
  }
  //  case pointsonly: {
  //    Space11_ knownSpace(*BT_);
  //    size_t K = getMeasurements()->size();
  //    calP_.reset(new set<size_t>());
  //    for(size_t k=0; k < K; k++) {
  //      MillerIndices<1> q = calQ_->at(k);
  //      PredictPointInSpace1 predictPointinSpace(q);
  //      size_t p = mapCalP_->at(k);
  //      double z_k = getMeasurement(k);
  //      double uprime = calUprime_->at(p);
  //      if(debug_)printf("p, k/K, q, z_k, uprime %lu, %lu/%lu, %d, %lf, %lf \n", p, k, K, q.at(0), z_k, uprime);
  //      assert(p < calUprime_->size());
  //      size_t key = Symbol('u', p);
  //      Double_ unknownuprimeEspression(key);
  //      calP_->insert(key);
  //      Double_ predictPointinSpaceExpression(predictPointinSpace, unknownuprimeEspression, knownSpace);
  //      graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
  //    }
  //    for(size_t p =0; p < calUprime_->size(); p++) {
  //      initial.insert(Symbol('u', p), 0.0);
  //    }
  //    break;
  //  }
  //
  //  case pointandlatticeonly: {
  //    Lattice1_ unknownLattice('l', 0);
  //    size_t K = getMeasurements()->size();
  //    calP_.reset(new set<size_t>());
  //    for(size_t k=0; k < K; k++) {
  //      MillerIndices<1> q = calQ_->at(k);
  //      PredictPointInSpaceGivenUnitCell1 predictPointinSpace(q, BT_->unitcell());
  //      size_t p = mapCalP_->at(k);
  //      double z_k = getMeasurement(k);
  //      double uprime = calUprime_->at(p);
  //      if(debug_)printf("p, k/K, q, z_k, uprime %lu, %lu/%lu, %d, %lf, %lf \n", p, k, K, q.at(0), z_k, uprime);
  //      assert(p < calUprime_->size());
  //      size_t key = Symbol('u', p);
  //      Double_ unknownuprimeEspression(key);
  //      calP_->insert(key);
  //      Double_ predictPointinSpaceExpression(predictPointinSpace, unknownuprimeEspression, unknownLattice);
  //      graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
  //    }
  //    initial.insert(Symbol('l',0), Lattice1());
  //    for(size_t p =0; p < calUprime_->size(); p++) {
  //      initial.insert(Symbol('u', p), 0.0);
  //    }
  //    break;
  //  }
  default:
    assert("Optimization Configuration Undefined! \n");
  }

  switch (optimizerFlag_) {
  case LM: {
    if(debug_) {
      LevenbergMarquardtParams params;
      params.setVerbosityLM("TRYDELTA");
      result = LevenbergMarquardtOptimizer(graph, initial,params).optimize();
    } else
      result = LevenbergMarquardtOptimizer(graph, initial).optimize();
    break;
  }
  case GN: {
    result = GaussNewtonOptimizer(graph, initial).optimize();
    break;
  }
  default:
    assert("Optimization Type Undefined! \n");
  }

  switch(flag){
  case spaceonly: {
    T_.reset(new UnitCell2(result.at<UnitCell2>(Symbol('x',0))));
    B_.reset(new Lattice1(result.at<Lattice1>(Symbol('l',0))));

    break;
  }
  case spaceandpoints: {
    result.print("Result as Values = \n");
    T_.reset(new UnitCell2(result.at<UnitCell2>(Symbol('x',0))));
    B_.reset(new Lattice1(result.at<Lattice1>(Symbol('l',0))));
    set<size_t>::iterator it = calP_->begin();
    calUprime_.reset(new vector<Eigen::Matrix<double, 1, 1> >());
    Eigen::Matrix<double, 1, 1> val;
    val << 0.0;
    calUprime_->push_back(val);
    for(; it != calP_->end() ; it++) {
      val << result.at<double>(*it);
      calUprime_->push_back(val);
    }

    calVprime_.reset(new vector<double>());
    calVprime_->push_back(0.0);
    it = calV_->begin();
    for(; it != calV_->end() ; it++)
      calVprime_->push_back(result.at<double>(*it));

    break;
  }
  //  case pointsonly: {
  //    result.print("Result as Values = \n");
  //    set<size_t>::iterator it = calP_->begin();
  //    calUprime_.reset(new vector<double>());
  //    for(; it != calP_->end() ; it++) {
  //      calUprime_->push_back(result.at<double>(*it));
  //      printf("result.at<double>(*it) = %lf", result.at<double>(*it));
  //    }
  //    break;
  //  }
  //  case pointandlatticeonly: {
  //    BT_.reset(new Space11(result.at<Lattice1>(Symbol('l',0)), BT_->unitcell()));
  //    result.print("Result as Values = \n");
  //    set<size_t>::iterator it = calP_->begin();
  //    calUprime_.reset(new vector<double>());
  //    for(; it != calP_->end() ; it++) {
  //      calUprime_->push_back(result.at<double>(*it));
  //      printf("result.at<double>(*it) = %lf", result.at<double>(*it));
  //    }
  //    break;
  //  }
  default:
    assert("Optimization Type Undefined! \n");
  }
  if(debug_) {
    cout << "Initial Error = " << graph.error(initial) << endl;
    cout << "Final Error = " << graph.error(result) << endl;
  }
}

NonlinearFactorGraph Estimate2DLattice2DSymmetry::createGraph(
    const double sigma, const OptimizeFlag flag) const {
  ExpressionFactorGraph graph;

  // measurement noise
  Isotropic::shared_ptr measurementNoise =
      Isotropic::Sigma(2, sigma);

  // create the graph
  switch(flag) {
  case spaceonly: {
    Lattice2_ unknowLattice('l', 0);
    UnitCell2_ unknowunitcell('x', 0);

    size_t K = getMeasurements()->size();
    for(size_t k=0; k < K; k++) {
      MillerIndices<2> q = calQ_->at(k);
      size_t p = f_->at(k);
      PredictPointInSpace22 predictPointinSpace(q);
      Point2 z_k = getMeasurement(k);
      Point2 uprime = Point2(calUprime_->at(p)(0), calUprime_->at(p)(1));
      assert(p < calUprime_->size());
      Point2_ uprimeExpression(uprime);
      Point2_ predictPointinSpaceExpression(predictPointinSpace, uprimeExpression, unknowLattice, unknowunitcell);
      graph.addExpressionFactor(predictPointinSpaceExpression, z_k, measurementNoise);
    }
    break;
  }

  default:
    assert("Optimization Configuration Undefined! \n");
  }

  return graph;
}

Values Estimate2DLattice2DSymmetry::initialEstimate(
    const OptimizeFlag flag) const {
  Values initial;

  switch (flag) {
  case spaceonly: {
    initial.insert(Symbol('l', 0), Lattice<2>(I_2x2));
    initial.insert(Symbol('x', 0), Pose2());
    break;
  }
  default:
    assert("Optimization Configuration Undefined! \n");
  }

  return initial;
}

Values Estimate2DLattice2DSymmetry::optimize(const double sigma,
    const OptimizeFlag flag) const {
  // factor graph
  NonlinearFactorGraph graph = createGraph(sigma, flag);

  // initial estimate
  Values initial = initialEstimate(flag);

  // Optimize
  Values result;
  switch (optimizerFlag_) {
  case LM: {
    LevenbergMarquardtParams params;
    if (debug_) {
      params.setVerbosityLM("TRYDELTA");
    }
    result = LevenbergMarquardtOptimizer(graph, initial, params).optimize();
    break;
  }
  case GN: {
    GaussNewtonParams params;
    if (debug_) {
      params.setVerbosity("DELTA");
    }
    result = GaussNewtonOptimizer(graph, initial, params).optimize();
    break;
  }
  default:
    assert("Optimization Type Undefined! \n");
  }

  if (debug_) {
    cout << "Initial Error = " << graph.error(initial) << endl;
    cout << "Final Error = " << graph.error(result) << endl;
  }

  return result;
}
