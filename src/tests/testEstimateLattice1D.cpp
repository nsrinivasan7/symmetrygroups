/*
 * testEstimateLattice1D.cpp
 *
 *  Created on: Oct 1, 2017
 *      Author: nsrinivasan7
 */

#include <array>

#include <gtsam/geometry/Pose3.h>
#include <CppUnitLite/TestHarness.h>

#include "../Predict.h"
#include "../IO.h"
#include "../Estimate.h"

using namespace std;
using namespace gtsam;

typedef pair<size_t, size_t> Indices;

/* ************************************************************************* */
// generate point in the unit cell
vector<double> getPointsinUnitCell() {
  vector<double> points;
  double radius = 0.3;
  while(radius < 0.8)
    points.push_back(radius+=0.1);
  return points;
}

/* ************************************************************************* */
vector<MillerIndices<1> > getMillerIndices(const size_t P, const size_t Q_1 = 5) {
  vector<MillerIndices<1> > q;
  int q_min =0;
  int q_max =0;

  if(Q_1 % 2 == 0) {
    q_min = -(Q_1 / 2 - 1);
    q_max = (Q_1 / 2);
  } else {
    q_min = -((Q_1-1) / 2);
    q_max = ( (Q_1-1) / 2);
  }

  for(int q_x = q_min; q_x <= q_max; q_x++)
    for(size_t p = 0; p < P; p++)
      q.push_back(MillerIndices<1>(q_x));
  return q;
}

/* ************************************************************************* */
vector<size_t> getPrimaryPointIndices(const size_t P, const size_t num_points) {
  vector<size_t> calP;
  for(size_t q_p = 0; q_p < num_points; q_p++) {
    calP.push_back(q_p % P);
  }
  return calP;
}

/* ************************************************************************* */
// generate points in lattice
vector<double> getPointsinLattice(Lattice<1>& lattice, const size_t repetitions_x = 5, vector<double> caluprimes = vector<double>(0)) {
  vector<double> uprimes;
  if(caluprimes.size() == 0) {
    vector<double> val = getPointsinUnitCell();
    uprimes.resize(val.size());
    copy(val.begin(), val.end(), uprimes.begin());
  } else {
    uprimes.resize(caluprimes.size());
    copy(caluprimes.begin(), caluprimes.end(), uprimes.begin());
  }

  vector<MillerIndices<1> > q = getMillerIndices(uprimes.size(), repetitions_x);
  vector<double> us;

  vector<MillerIndices<1> >::iterator it = q.begin();
  for(size_t repetition = 0; repetition < repetitions_x; repetition++) {
    for(size_t p = 0; p < uprimes.size(); p++) {
      double uprime = uprimes.at(p);
      double u = uprime + (double)(it->at(0));
      us.push_back(u);
      it++;
    }
  }
  return us;
}

/* ************************************************************************* */
// rotate and translate the points by the pose of the unit cell
vector<double> getPoints(Space1<1>& space, size_t num_rep_x = 5, vector<double> caluprimes = vector<double>(0)) {
  Lattice1 lattice = space.lattice();
  vector<double> us = getPointsinLattice(lattice , num_rep_x, caluprimes);
  const UnitCell1 unitcell = space.unitcell();
  vector<double> xs;
  for(size_t j = 0; j < us.size(); j++) {
    double x = us.at(j) * lattice.basis()(0) + unitcell;
    xs.push_back(x);
  }
  return xs;
}

//******************************************************************************
TEST(getPoints , function1d) {
  Vector1 b1; b1 << 1.0;
  Lattice<1> lattice(b1);
  UnitCell1 unitcell = 2.5;
  Space1<1> space(lattice, unitcell);
  size_t num_rep_x = 5;
  vector<double> points = getPoints(space, num_rep_x);
//  const string filename = "../test_data/simulatedSpace11.txt";
//  write_values_to_ascii_file(filename, points);
}

//******************************************************************************
TEST(estimate1dlattice, basis) {
  Vector1 b1; b1 << 1.0;
  Lattice<1> lattice(b1);
  UnitCell1 unitcell = 2.5;
  Space1<1> space(lattice, unitcell);
  size_t num_rep_x = 5;
  vector<double> calZ = getPoints(space, num_rep_x); printf("Number of Measurements =%u\n", calZ.size());
  vector<double> calUprime = getPointsinUnitCell(); printf("Number of Points in Unit Cell  =%u\n", calUprime.size());
  vector<MillerIndices<1> > calQ = getMillerIndices(calUprime.size(), num_rep_x); printf("Number of Miller indices  =%u\n", calQ.size());
  vector<size_t> calP = getPrimaryPointIndices(calUprime.size(), calQ.size());
  Estimate1DLattice estimate(calZ, calP, calQ, calUprime);
  estimate.optimizeBT();
  boost::shared_ptr<Space11> actual(new Space11());
  estimate.getBT(actual);
  actual->print("BT= \n");
//  vector<double> points = getPoints(actual, num_rep_x);
//  const string filename = "../test_data/simulatedSpace11actual.txt";
//    write_values_to_ascii_file(filename, points);
}

////******************************************************************************
TEST(estimate1dlattice, basisAndPoints) {
  Vector1 b1; b1 << 1.0;
  Lattice<1> lattice(b1);
  UnitCell1 unitcell = 1.0;
  Space1<1> space(lattice, unitcell);
  size_t num_rep_x = 5;
  vector<double> calZ = getPoints(space, num_rep_x); printf("Number of Measurements =%u\n", calZ.size());
  vector<double> calUprime = getPointsinUnitCell(); printf("Number of Points in Unit Cell  =%u\n", calUprime.size());
  vector<MillerIndices<1> > calQ = getMillerIndices(calUprime.size(), num_rep_x); printf("Number of Miller indices  =%u\n", calQ.size());
  vector<size_t> calP = getPrimaryPointIndices(calUprime.size(), calQ.size());
  Estimate1DLattice estimate(calZ, calP, calQ, calUprime);
  estimate.optimizeBTandPoints();
  boost::shared_ptr<Space11> actual_BT(new Space11());
  estimate.getBT(actual_BT);
  boost::shared_ptr<vector<double> > uprimes_actual(new vector<double>);
  estimate.getUprimes(uprimes_actual);
  actual_BT->print("BT= \n");
  vector<double> points = getPoints(*actual_BT, num_rep_x, *uprimes_actual);
  for(size_t j = 0; j < points.size(); j++)
     printf("z(%lf) @ uprime(%lf) B(%lf) t(%lf)[Initial] z(%lf) @ uprime(%lf) B(%lf) t(%lf)[Optimized], q= %d \n", calZ.at(j), calUprime.at(calP.at(j)), b1(0), unitcell, points.at(j), uprimes_actual->at(calP.at(j)), actual_BT->lattice().basis()(0), actual_BT->unitcell(), calQ.at(j).at(0));
//  const string filename = "../test_data/simulatedSpace11actual.txt";
//    write_values_to_ascii_file(filename, points);
}

////******************************************************************************
TEST(estimate1dlattice, points) {
  Vector1 b1; b1 << 2.0;
  Lattice<1> lattice(b1);
  UnitCell1 unitcell = 1.0;
  Space1<1> space(lattice, unitcell);
  size_t num_rep_x = 5;
  vector<double> calZ = getPoints(space, num_rep_x); printf("Number of Measurements =%u\n", calZ.size());
  vector<double> calUprime = getPointsinUnitCell(); printf("Number of Points in Unit Cell  =%u\n", calUprime.size());
  vector<MillerIndices<1> > calQ = getMillerIndices(calUprime.size(), num_rep_x); printf("Number of Miller indices  =%u\n", calQ.size());
  vector<size_t> calP = getPrimaryPointIndices(calUprime.size(), calQ.size());
  Estimate1DLattice estimate(calZ, calP, calQ, calUprime);
  estimate.SetLatticeAndUnitCell(space);
  estimate.optimizePointsAndLattice();
  boost::shared_ptr<Space11> actual_BT(new Space11());
  estimate.getBT(actual_BT);
  boost::shared_ptr<vector<double> > uprimes_actual(new vector<double>);
  estimate.getUprimes(uprimes_actual);
  actual_BT->print("BT= \n");
  vector<double> points = getPoints(*actual_BT, num_rep_x, *uprimes_actual);
//  for(size_t j = 0; j < points.size(); j++)
//     printf("z(%lf) @ uprime(%lf) B(%lf) t(%lf)[Initial] z(%lf) @ uprime(%lf) B(%lf) t(%lf)[Optimized], q= %d \n", calZ.at(j), calUprime.at(calP.at(j)), b1(0), unitcell, points.at(j), uprimes_actual->at(calP.at(j)), actual_BT->lattice().basis()(0), actual_BT->unitcell(), calQ.at(j).at(0));
//  const string filename = "../test_data/simulatedSpace11actual.txt";
//    write_values_to_ascii_file(filename, points);
}


/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */



