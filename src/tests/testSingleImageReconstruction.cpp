/*
 * testSingleImageReconstruction.cpp
 *
 *  Created on: May, 2017
 *      Author: Natesh Srinivasan
 *      @brief :Single Image Reconstruction
 */
#include <vector>
#include <iostream>

#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/expressions.h>

#include <CppUnitLite/TestHarness.h>

#include "../Predict.h"
#include "../Space.h"
#include "../Expressions.h"

using namespace std;
using namespace gtsam;
using namespace noiseModel;

// create a camera pose. facing the (0,0,0) and at 10m depth
gtsam::Pose3 camerapose(gtsam::Rot3(),gtsam::Point3(0,0,-10));

// specify the calibration
gtsam::Cal3_S2 calibration(50.0,50.0,0.0,25.0,25.0);

// the camera
gtsam::SimpleCamera camera = gtsam::SimpleCamera::Lookat(gtsam::Point3(-10,0,-10),gtsam::Point3(0.0,0.0,0.0), gtsam::Point3(0.0,-1.0,0.0),calibration);

// create the 3D points on the facade
// width of the window is 1m, height is 2m. center of the first element
// passing along the principal axis

#define WINDOW_WIDTH 1.0
#define WINDOW_HEIGHT 2.0
#define WINDOWS_SEPERATION 3.0

// square window with the depth (z) of the corners at 0.1,0.2,0.3,0.4

std::vector<gtsam::Point3> generatex() {

  std::vector<gtsam::Point3> results;

  //center window corresponding to the unit cell
  // x to the right, y to the bottom and z to the camera away from you

  //top left
  results.push_back(gtsam::Point3(-WINDOW_WIDTH/2, -WINDOW_HEIGHT/2, -0.1 ));
  //top right
  results.push_back(gtsam::Point3(WINDOW_WIDTH/2, -WINDOW_HEIGHT/2, -0.2 ));
  // bottom right
  results.push_back(gtsam::Point3(WINDOW_WIDTH/2, WINDOW_HEIGHT/2, -0.3 ));
  //bottom left
  results.push_back(gtsam::Point3(-WINDOW_WIDTH/2, WINDOW_HEIGHT/2, -0.4 ));

  //left windows corners

  //top left
  results.push_back(gtsam::Point3(-WINDOW_WIDTH/2 - WINDOWS_SEPERATION, -WINDOW_HEIGHT/2, -0.1 ));
  //top right
  results.push_back(gtsam::Point3(WINDOW_WIDTH/2  - WINDOWS_SEPERATION, -WINDOW_HEIGHT/2, -0.2 ));
  // bottom right
  results.push_back(gtsam::Point3(WINDOW_WIDTH/2  - WINDOWS_SEPERATION, WINDOW_HEIGHT/2, -0.3 ));
  //bottom left
  results.push_back(gtsam::Point3(-WINDOW_WIDTH/2 - WINDOWS_SEPERATION, WINDOW_HEIGHT/2, -0.4 ));

  //right windows corners

  //top left
  results.push_back(gtsam::Point3(-WINDOW_WIDTH/2 + WINDOWS_SEPERATION, -WINDOW_HEIGHT/2, -0.1 ));
  //top right
  results.push_back(gtsam::Point3(WINDOW_WIDTH/2  + WINDOWS_SEPERATION, -WINDOW_HEIGHT/2, -0.2 ));
  // bottom right
  results.push_back(gtsam::Point3(WINDOW_WIDTH/2  + WINDOWS_SEPERATION, WINDOW_HEIGHT/2, -0.3 ));
  //bottom left
  results.push_back(gtsam::Point3(-WINDOW_WIDTH/2 + WINDOWS_SEPERATION, WINDOW_HEIGHT/2, -0.4 ));

  return results;

}

//create uprimes corresponding to the above points
std::vector<double> generateuprimes() {

  std::vector<double> results;

  //center
  //top left
  results.push_back((-WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  //top right
  results.push_back((WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  // bottom right
  results.push_back((WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  //bottom left
  results.push_back((-WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));

  //left
  //top left
  results.push_back((-WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  //top right
  results.push_back((WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  // bottom right
  results.push_back((WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  //bottom left
  results.push_back((-WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));

  //right
  //top left
  results.push_back((-WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  //top right
  results.push_back((WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  // bottom right
  results.push_back((WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));
  //bottom left
  results.push_back((-WINDOW_WIDTH/(2*WINDOWS_SEPERATION)));

  return results;

}

std::vector<Eigen::Matrix<double,2,1> > generatez() {

  std::vector<Eigen::Matrix<double,2,1> > results;

  Eigen::Matrix<double,2,1> val;
  //center
  //top left
  val << -WINDOW_HEIGHT/2, -0.1;
  results.push_back(val);
  //top right
  val << -WINDOW_HEIGHT/2, -0.2;
  results.push_back(val);
  // bottom right
  val << WINDOW_HEIGHT/2, -0.3;
  results.push_back(val);
  //bottom left
  val << WINDOW_HEIGHT/2, -0.4;
  results.push_back(val);

  //left
  //top left
  val << -WINDOW_HEIGHT/2, -0.1;
  results.push_back(val);
  //top right
  val << -WINDOW_HEIGHT/2, -0.2;
  results.push_back(val);
  // bottom right
  val << WINDOW_HEIGHT/2, -0.3;
  results.push_back(val);
  //bottom left
  val << WINDOW_HEIGHT/2, -0.4;
  results.push_back(val);

  //right
  //top left
  val << -WINDOW_HEIGHT/2, -0.1;
  results.push_back(val);
  //top right
  val << -WINDOW_HEIGHT/2, -0.2;
  results.push_back(val);
  // bottom right
  val << WINDOW_HEIGHT/2, -0.3;
  results.push_back(val);
  //bottom left
  val << WINDOW_HEIGHT/2, -0.4;
  results.push_back(val);

  return results;

}


//project the measurement onto camera
std::vector<gtsam::Point2> createMeasurements() {

  std::vector<gtsam::Point2> measurements;

  std::vector<gtsam::Point3> corners = generatex();

  for(size_t i = 0; i < corners.size(); i++)
    measurements.push_back(camera.project(corners.at(i)));
  return measurements;
}

// create ground truth correspondences
std::vector<std::pair<size_t,size_t> > createMeasurementpairs() {
  std::vector<std::pair<size_t,size_t> > indices;

  //top left
  indices.push_back(make_pair(0,4));
  indices.push_back(make_pair(0,8));

  //top right
  indices.push_back(make_pair(1,5));
  indices.push_back(make_pair(1,9));

  //bottom right
  indices.push_back(make_pair(2,6));
  indices.push_back(make_pair(2,10));

  //bottom left
  indices.push_back(make_pair(3,7));
  indices.push_back(make_pair(3,11));

  return indices;
}

// miller indices
std::vector<int> createMillerindices() {
  std::vector<int> indices;

  indices.push_back(0);
  indices.push_back(0);
  indices.push_back(0);
  indices.push_back(0);

  indices.push_back(-1);
  indices.push_back(-1);
  indices.push_back(-1);
  indices.push_back(-1);

  indices.push_back(1);
  indices.push_back(1);
  indices.push_back(1);
  indices.push_back(1);



  return indices;

}

TEST(SingleImageReconstruction, verifydata)
{

  // specify the points in the unit cell
  std::vector<double> uprimes = generateuprimes();

  //specify a 1D lattice with the basis vector
  Eigen::Matrix<double, 1, 1> basis; basis << WINDOWS_SEPERATION;
  Lattice<1> lattice(basis);

  //specify the pose of the unit cell
  UnitCell unitcell;

  std::vector<gtsam::Point3> actualPoints = generatex();
  std::vector<gtsam::Point3> expectedPoints(actualPoints.size());

  const int k = 3; const int n = 1;
  Space<k, n> space(lattice, Pose3());
  std::vector<Eigen::Matrix<double,2,1> > z  = generatez();
  vector<int> millerindices = createMillerindices();

  for(size_t i = 0; i < expectedPoints.size(); i++)  {

    // x = [B(q+uprime)|y|z];
    Space<k,n>::PointN u; u << uprimes[i] + (double)millerindices[i];
    Space<k,n>::PointK x = space.getx(u,z[i]);
    expectedPoints[i] = gtsam::Point3(x(0,0),x(1,0),x(2,0));
    EXPECT(assert_equal(expectedPoints[i], actualPoints[i]));
  }
}

TEST(SingleImageReconstruction, optizecamerapose)
{

  gtsam::ExpressionFactorGraph graph;

  // measurement noise
  Isotropic::shared_ptr measurementNoise =
      Isotropic::Sigma(2, 1.0); // one pixel in u and v

  //get the measurements that are projected onto the camera
  std::vector<gtsam::Point2>measurements =  createMeasurements();

  // get the measurement pairs
  std::vector<std::pair<size_t,size_t> >pairs =  createMeasurementpairs();

  //get the measurement indices
  std::vector<int> millerindices = createMillerindices();

  //the camera pose that we are optimizing for
  Pose3_ unnowncameraPose('x',0);

  //specify a 1D lattice with the basis vector
  Eigen::Matrix<double, 1, 1> basis; basis << WINDOWS_SEPERATION;
  Lattice<1> lattice(basis);
  //specify the pose of the unit cell
  UnitCell unitcell;

  const int k = 3; const int n = 1;
  Space<k, n> space(lattice, Pose3());

  Space31_ knownspace(space);
  Cal3_S2_ knownCameraCalibration(calibration);

  std::vector<Eigen::Matrix<double,2,1> > z  = generatez();

  for(size_t i = 0; i < pairs.size(); i++) {



//    Point3_ pointExpression(PredictPointInSpace<k, n>(millerindices[i],z[i]), uprime , space );

//    // get the measurement noise
//    Point2_ prediction = uncalibrate(knownCameraCalibration, project(transform_to(unnowncameraPose,displacedPointExpression)));

//    Point2 uv = measurements.at(pairs.at(i).second);

//    Predict1D v(indices.at(i));
//    graph.addExpressionFactor(prediction, uv, measurementNoise);
  }

//  //add a prior for the pose
//  Vector6 sigmas; sigmas << Vector3(0.3,0.3,0.3), Vector3(0.1,0.1,0.1);
//  Diagonal::shared_ptr poseNoise = Diagonal::Sigmas(sigmas);
//  Pose3_ x('x',0);
//  graph.addExpressionFactor(x, camera.pose(), poseNoise);
//
//
//  // create a perturbed initial value
//  Values initial,result;
//
//
//  initial.insert(Symbol('x',0), gtsam::Pose3(gtsam::Rot3(), gtsam::Point3(0.0,0.0,-12.0)));
//  initial.insert(Symbol('l',0), gtsam::Point3(-0.54,-1.12,0.0));
//  initial.insert(Symbol('l',1), gtsam::Point3(0.51,-1.09,0.0));
//  initial.insert(Symbol('l',2), gtsam::Point3(0.53,1.13,0.0));
//  initial.insert(Symbol('l',3), gtsam::Point3(-0.50,1.3,0.0));
//
//
//  try{
//    result = DoglegOptimizer(graph, initial).optimize();
//  }catch(std::exception& e){
//    std::cout << e.what() << "\n";
//  }
//
//  gtsam::Pose3 actual = result.at<gtsam::Pose3>(Symbol('x',0));
//  EXPECT(assert_equal(camera.pose(), actual ,1e-5));
}

//TEST(SingleImageReconstruction, PointUnknownTxunknwon)
//{
//
//  gtsam::ExpressionFactorGraph graph;
//
//  // measurement noise
//  Isotropic::shared_ptr measurementNoise =
//      Isotropic::Sigma(2, 1.0); // one pixel in u and v
//
//  // get the points, in this case, let us assume that we know the location of the points
//  std::vector<gtsam::Point3> corners =createPoints();
//
//  //get the measurements that are projected onto the camera
//  std::vector<gtsam::Point2> measurements = createMeasurments();
//
//  // get the measurement pairs
//  std::vector<std::pair<size_t,size_t> >pairs =  createMeasurementpairs();
//
//  //get the measurement indices
//  std::vector<size_t> indices =createMeasurementindices();
//
//  //the camera pose that we are optimizing for
//  Pose3_ unnowncameraPose('x',0);
//
//  double_ unknownTranslationExpression('t',0);
//
//  Cal3_S2_ knownCameraCalibration(calibration);
//
//  for(size_t i = 0; i < pairs.size(); i++) {
//
//    //add the known Point expression
//    Point3_ unknownPointExpression(Symbol('l',pairs.at(i).first));
//
//    Point3_ displacedPointExpression(Predict1D(indices.at(i)), unknownPointExpression, unknownTranslationExpression);
//
//    // get the measurement noise
//    Point2_ prediction = uncalibrate(knownCameraCalibration, project(transform_to(unnowncameraPose,displacedPointExpression)));
//
//    Point2 uv = measurements.at(pairs.at(i).second);
//
//    Predict1D v(indices.at(i));
//    graph.addExpressionFactor(prediction, uv, measurementNoise);
//  }
//
//  //add a prior for the pose
//  Vector6 sigmas; sigmas << Vector3(0.3,0.3,0.3), Vector3(0.1,0.1,0.1);
//  Diagonal::shared_ptr poseNoise = Diagonal::Sigmas(sigmas);
//  graph.addExpressionFactor(unnowncameraPose, camera.pose(), poseNoise);
//
//
//  //add a prior for the translation
//  Vector1 sig; sig << 1.0;
//  Diagonal::shared_ptr tranNoise = Diagonal::Sigmas(sig);
//  graph.addExpressionFactor(unknownTranslationExpression, 2.3, tranNoise);
//
//  // create a perturbed initial value
//  Values initial,result;
//
//
//  initial.insert(Symbol('x',0), gtsam::Pose3(gtsam::Rot3(), gtsam::Point3(0.0,0.0,-12.0)));
//  initial.insert(Symbol('l',0), gtsam::Point3(-0.54,-1.12,0.0));
//  initial.insert(Symbol('l',1), gtsam::Point3(0.51,-1.09,0.0));
//  initial.insert(Symbol('l',2), gtsam::Point3(0.53,1.13,0.0));
//  initial.insert(Symbol('l',3), gtsam::Point3(-0.50,1.3,0.0));
//  initial.insert(Symbol('t',0), 2.7);
//
//
//  try{
//    result = DoglegOptimizer(graph, initial).optimize();
//  }catch(std::exception& e){
//    std::cout << e.what() << "\n";
//  }
//
//  gtsam::Pose3 actual = result.at<gtsam::Pose3>(Symbol('x',0));
//  EXPECT(assert_equal(camera.pose(), actual ,1e-5));
//}

//TEST(SingleImageReconstruction, PointUnknownTxunknwonKunknown)
//{
//
//  gtsam::ExpressionFactorGraph graph;
//
//  // measurement noise
//  Isotropic::shared_ptr measurementNoise =
//      Isotropic::Sigma(2, 1.0); // one pixel in u and v
//
//  // get the points, in this case, let us assume that we know the location of the points
//  std::vector<gtsam::Point3> corners =createPoints();
//
//  //get the measurements that are projected onto the camera
//  std::vector<gtsam::Point2> measurements = createMeasurments();
//
//  // get the measurement pairs
//  std::vector<std::pair<size_t,size_t> >pairs =  createMeasurementpairs();
//
//  //get the measurement indices
//  std::vector<size_t> indices =createMeasurementindices();
//
//  //the camera pose that we are optimizing for
//  Pose3_ unnowncameraPose('x',0);
//
//  double_ unknownTranslationExpression('t',0);
//
//  Cal3_S2_ unknownCameraCalibration('k',0);
//
//  for(size_t i = 0; i < pairs.size(); i++) {
//
//    //add the known Point expression
//    Point3_ unknownPointExpression(Symbol('l',pairs.at(i).first));
//
//    Point3_ displacedPointExpression(Predict1D(indices.at(i)), unknownPointExpression, unknownTranslationExpression);
//
//    // get the measurement noise
//    Point2_ prediction = uncalibrate(unknownCameraCalibration, project(transform_to(unnowncameraPose,displacedPointExpression)));
//
//    Point2 uv = measurements.at(pairs.at(i).second);
//
//    Predict1D v(indices.at(i));
//    graph.addExpressionFactor(prediction, uv, measurementNoise);
//  }
//
//  //add a prior for the pose
//  Vector6 sigmas; sigmas << Vector3(0.3,0.3,0.3), Vector3(0.1,0.1,0.1);
//  Diagonal::shared_ptr poseNoise = Diagonal::Sigmas(sigmas);
//  graph.addExpressionFactor(unnowncameraPose, camera.pose(), poseNoise);
//
//
//  //add a prior for the translation
//  Vector1 sig; sig << 1.0;
//  Diagonal::shared_ptr tranNoise = Diagonal::Sigmas(sig);
//  graph.addExpressionFactor(unknownTranslationExpression, 2.3, tranNoise);
//
//  // add prior on the calibration
//  Vector5 sigcal; sigcal << 10.0,10.0,10.0,10.0,10.0;
//  Diagonal::shared_ptr calNoise = Diagonal::Sigmas(sigcal);
//  graph.addExpressionFactor(unknownCameraCalibration, calibration, calNoise);
//
//  // create a perturbed initial value
//  Values initial,result;
//
//
//  initial.insert(Symbol('x',0), gtsam::Pose3(gtsam::Rot3(), gtsam::Point3(0.0,0.0,-12.0)));
//  initial.insert(Symbol('l',0), gtsam::Point3(-0.54,-1.12,0.0));
//  initial.insert(Symbol('l',1), gtsam::Point3(0.51,-1.09,0.0));
//  initial.insert(Symbol('l',2), gtsam::Point3(0.53,1.13,0.0));
//  initial.insert(Symbol('l',3), gtsam::Point3(-0.50,1.3,0.0));
//  initial.insert(Symbol('t',0), 2.7);
//  initial.insert(Symbol('k',0), calibration);
//
//  try{
//    result = DoglegOptimizer(graph, initial).optimize();
//  }catch(std::exception& e){
//    std::cout << e.what() << "\n";
//  }
//
//  gtsam::Pose3 actual = result.at<gtsam::Pose3>(Symbol('x',0));
//  EXPECT(assert_equal(camera.pose(), actual ,1e-5));
//
//}
//
//TEST(SingleImageReconstruction, PointUnknownTxunknwonKunknown3D)
//{
//
//  gtsam::ExpressionFactorGraph graph;
//
//  // measurement noise
//  Isotropic::shared_ptr measurementNoise =
//      Isotropic::Sigma(2, 1.0); // one pixel in u and v
//
//  // get the points, in this case, let us assume that we know the location of the points
//  std::vector<gtsam::Point3> corners =createPoints();
//
//  //get the measurements that are projected onto the camera
//  std::vector<gtsam::Point2> measurements = createMeasurments();
//
//  // get the measurement pairs
//  std::vector<std::pair<size_t,size_t> >pairs =  createMeasurementpairs();
//
//  //get the measurement indices
//  std::vector<size_t> indices =createMeasurementindices();
//
//  //the camera pose that we are optimizing for
//  Pose3_ unknowncameraPose('x',0);
//
//  Point3_ unknownTranslationExpression('t',0);
//
//  Cal3_S2_ unknownCameraCalibration('k',0);
//
//  for(size_t i = 0; i < pairs.size(); i++) {
//
//    //add the known Point expression
//    Point3_ unknownPointExpression(Symbol('l',pairs.at(i).first));
//
//    Point3_ displacedPointExpression(Predict1DR(indices.at(i)), unknownPointExpression, unknownTranslationExpression);
//
//    // get the measurement noise
//    Point2_ prediction = uncalibrate(unknownCameraCalibration, project(transform_to(unknowncameraPose,displacedPointExpression)));
//
//    Point2 uv = measurements.at(pairs.at(i).second);
//
//    Predict1D v(indices.at(i));
//    graph.addExpressionFactor(prediction, uv, measurementNoise);
//  }
//
//  //add a prior for the pose
//  Vector6 sigmas; sigmas << Vector3(0.3,0.3,0.3), Vector3(0.1,0.1,0.1);
//  Diagonal::shared_ptr poseNoise = Diagonal::Sigmas(sigmas);
//  graph.addExpressionFactor(unknowncameraPose, camera.pose(), poseNoise);
//
//
//  //add a prior for the translation
//  Vector3 sig; sig << 1.0,1.0,1.0;
//  Diagonal::shared_ptr tranNoise = Diagonal::Sigmas(sig);
//  graph.addExpressionFactor(unknownTranslationExpression, gtsam::Point3(2.3,0.0,0.0), tranNoise);
//
//  // add prior on the calibration
//  Vector5 sigcal; sigcal << 10.0,10.0,10.0,10.0,10.0;
//  Diagonal::shared_ptr calNoise = Diagonal::Sigmas(sigcal);
//  graph.addExpressionFactor(unknownCameraCalibration, calibration, calNoise);
//
//  // create a perturbed initial value
//  Values initial,result;
//
//
//  initial.insert(Symbol('x',0), gtsam::Pose3(gtsam::Rot3(), gtsam::Point3(0.0,0.0,-12.0)));
//  initial.insert(Symbol('l',0), gtsam::Point3(-0.54,-1.12,0.0));
//  initial.insert(Symbol('l',1), gtsam::Point3(0.51,-1.09,0.0));
//  initial.insert(Symbol('l',2), gtsam::Point3(0.53,1.13,0.0));
//  initial.insert(Symbol('l',3), gtsam::Point3(-0.50,1.3,0.0));
//  initial.insert(Symbol('t',0), gtsam::Point3(2.7,0.0,0.0));
//  initial.insert(Symbol('k',0), calibration);
//
//  try{
//    result = DoglegOptimizer(graph, initial).optimize();
//  }catch(std::exception& e){
//    std::cout << e.what() << "\n";
//  }
//
//  gtsam::Pose3 actual = result.at<gtsam::Pose3>(Symbol('x',0));
//  EXPECT(assert_equal(camera.pose(), actual ,1e-5));

//}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */


