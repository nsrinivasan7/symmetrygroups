/*
 * testMillerIndices.cpp
 *
 *  Created on: Apr 25, 2017
 *      Author: nsrinivasan7
 */



#include "../MillerIndices.h"

#include <CppUnitLite/TestHarness.h>
#include <gtsam/geometry/Pose3.h>

using namespace std;
using namespace gtsam;

//******************************************************************************
TEST(MillerIndices , Constructor) {

  MillerIndices<3> ijk(1, 2, 3);

  Eigen::Matrix<int,3,1> expected;
  expected << 1, 2, 3;

  Eigen::Matrix<int,3,1> actual = ijk.indices();

  CHECK_EQUAL(expected,actual);

};

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
