/*
 * test2DSymmetries.cpp
 *
 *  Created on: Apr 20, 2017
 *      Author: nsrinivasan7
 */
#include <array>

#include <gtsam/geometry/Pose3.h>
#include <CppUnitLite/TestHarness.h>

#include "../Predict.h"
#include "../IO.h"
#include "../Estimate.h"

using namespace std;
using namespace gtsam;

/* ************************************************************************* */
// generate an example constellation of points in a unit cell
// for K=1, it has 3 points
// for K=2, it has 4 points
template<int K>
vector<Eigen::Matrix<double, K ,1> > generateUPrimeExample() {
  vector<Eigen::Matrix<double, K ,1> > calUprime;
  if(K == 1) {
    for(double x = 0.0; x < 0.75; x+=0.250) {
      Eigen::Matrix<double, K ,1> uprime; uprime << x;
      calUprime.push_back(uprime);
    }
  }

  if(K == 2) {
    for(double y = 0.0; y < 0.5; y+=0.250)
      for(double x = 0.0; x < 0.5; x+=0.250) {
        Eigen::Matrix<double, K, 1> uprime; uprime << x, y;
        calUprime.push_back(uprime);
      }
  }
  return calUprime;
}

template<int K>
vector<double> getVprime(const vector<Eigen::Matrix<double, K ,1> >& calUprime, const Lattice<K>& lattice) {
  vector<double> calVprime;
  if(K == 2)
    return calVprime;
  if(K == 1) {
    for(size_t p = 0; p < calUprime.size(); p++) {
      double vprime = (calUprime.at(p)(0) + 0.1) * lattice.basis()(0);
      calVprime.push_back(vprime);
    }
    return calVprime;
  }
}

template<int K>
vector<MillerIndices<K> > generateMillerIndices(const size_t P, const size_t Q_1 = 5,
    const size_t Q_2 =5) {

  vector<MillerIndices<K> > Q;

  if(K == 1) {
    int q_min =0;
    int q_max =0;

    if(Q_1 % 2 == 0) {
      q_min = -(Q_1 / 2 - 1);
      q_max = (Q_1 / 2);
    } else {
      q_min = -((Q_1-1) / 2);
      q_max = ( (Q_1-1) / 2);
    }

    for(int q_x = q_min; q_x <= q_max; q_x++)
      for(size_t p = 0; p < P; p++)
        Q.push_back(MillerIndices<K>(q_x));
  }

  if(K == 2) {
    int q_min_x =0;
    int q_min_y =0;
    int q_max_x =0;
    int q_max_y =0;

    if(Q_1 % 2 == 0) {
      q_min_x = -(Q_1 / 2 - 1);
      q_max_x = (Q_1 / 2);
    } else {
      q_min_x = -((Q_1-1) / 2);
      q_max_x = ( (Q_1-1) / 2);
    }

    if(Q_2 % 2 == 0) {
      q_min_y = -(Q_2 / 2 - 1);
      q_max_y = (Q_2 / 2);
    } else {
      q_min_y = -((Q_2-1) / 2);
      q_max_y = ( (Q_2-1) / 2);
    }

    for(int q_x = q_min_x; q_x <= q_max_x; q_x++)
      for(int q_y = q_min_y; q_y <= q_max_y; q_y++)
        for(size_t p = 0; p < P; p++)
          Q.push_back(MillerIndices<K>(q_x, q_y));
  }
  return Q;
}

/* ************************************************************************* */
vector<size_t> getPrimaryPointIndices(const size_t P, const size_t num_points) {
  vector<size_t> J;
  for(size_t q_p = 0; q_p < num_points; q_p++) {
    J.push_back(q_p % P);
  }
  return J;
}

/* ************************************************************************* */
// generate points in lattice
template<int K>
vector<Eigen::Matrix<double, K ,1> > getPointsinLattice(const size_t repetitions_x = 5, const size_t repetitions_y =5) {

  vector<Eigen::Matrix<double, K ,1> > calUprime = generateUPrimeExample<K>();
  vector<MillerIndices<K> > Q = generateMillerIndices<K>(calUprime.size(), repetitions_x, repetitions_y);
  vector<Eigen::Matrix<double, K ,1> > calU;
  size_t P = calUprime.size();

  if(K == 1) {
    int k = 0;
    for(typename vector<MillerIndices<K> >::iterator it = Q.begin(); it != Q.end(); it++) {
      Eigen::Matrix<double, K ,1> u;
      u << calUprime.at(k % P)(0) + it->at(0);
      calU.push_back(u);
      k++;
    }
  }

  if(K == 2) {
    int k = 0;
    for(typename vector<MillerIndices<K> >::iterator it = Q.begin(); it != Q.end(); it++) {
      Eigen::Matrix<double, K, 1> u;
      Eigen::Matrix<double, K, 1> uprime = calUprime.at(k % P);
      Eigen::Matrix<int, K, 1> q_j_int = it->getIndices();
      Eigen::Matrix<double, K, 1> q_j = q_j_int.template cast <double> ();
      u = uprime + q_j;
      calU.push_back(u);
      k++;
    }
  }

  return calU;
}

// rotate and translate the points by the pose of the unit cell
template<int K>
vector<Point2> getPoints(const Lattice<K>& lattice, const UnitCell2& unitCell, size_t num_rep_x = 5, size_t num_rep_y = 5) {
  vector<Point2> calX;
  vector<Eigen::Matrix<double, K ,1> > calU = getPointsinLattice<K>(num_rep_x, num_rep_y);

  if(K == 1) {
    const vector<Eigen::Matrix<double, K, 1> > calUprime = generateUPrimeExample<K>();
    vector<double> Vprime = getVprime<K>(calUprime, lattice);
    size_t P = calUprime.size();
    for(size_t j = 0; j < calU.size(); j++) {
      Point2 w(calU.at(j)(0)* lattice.basis()(0), Vprime.at(j % P) );
      //      Point2 w(calU.at(j)(0)* lattice.basis()(0), 0.0 );
      Point2 x = unitCell.transform_from(w);
      calX.push_back(x);
    }
  }

  if(K == 2) {
    for(size_t j = 0; j < calU.size(); j++) {
      Point2 u(calU.at(j)(0), calU.at(j)(1));
      Point2 x = unitCell.transform_from(u);
      calX.push_back(x);
    }
  }
  return calX;
}

//******************************************************************************
TEST(getPoints , unitCell) {
  const int K = 1;
  vector<Eigen::Matrix<double, K, 1> > calUprime = generateUPrimeExample<K>();
  Vector1 b1; b1 << 2.0;
  Lattice<K> lattice(b1);
  vector<double> Vprime = getVprime<K>(calUprime, lattice);

  EXPECT(calUprime.size() != 0)
  if(K == 1) {
    vector<double> values;
    vector<Point2> uv;
    for(size_t p =0; p < calUprime.size(); p++) {
      values.push_back(calUprime.at(p)(0));
      uv.push_back(Point2(calUprime.at(p)(0), Vprime.at(p)));
    }

    const string filename = "../test_data/simulatedSpace21UnitCell.txt";
    const string filename2 = "../test_data/simulatedSpace21UnitCell.txt";
    write_values_to_ascii_file(filename, values);
    write_points_to_ascii_file(filename2, uv);
  }
  const int K2 = 2;
  vector<Eigen::Matrix<double, K2, 1> > calUprime2 = generateUPrimeExample<K2>();
  EXPECT(calUprime2.size() != 0)
  if(K2 == 2) {
    vector<Point2> values;
    for(size_t p =0; p < calUprime2.size(); p++)
      values.push_back(Point2(calUprime2.at(p)(0), calUprime2.at(p)(1)));
    const string filename = "../test_data/simulatedSpace22UnitCell.txt";
    write_points_to_ascii_file(filename, values);
  }
}

//******************************************************************************
TEST(getPoints , lattice) {
  const int K = 1;
  Vector1 b1; b1 << 0.5;
  Lattice<K> lattice(b1);
  size_t num_rep_x = 5;

  vector<Eigen::Matrix<double, K, 1> > calUprime = generateUPrimeExample<K>();
  vector<Eigen::Matrix<double, K ,1> > calU  = getPointsinLattice<K>(num_rep_x);
  vector<double> Vprime = getVprime<K>(calUprime, lattice);

  EXPECT(calU.size() != 0)
  EXPECT(calUprime.size() != 0)

  if(K == 1) {
    vector<Point2> uv;
    for(size_t j =0; j < calU.size(); j++)
      uv.push_back(Point2(calU.at(j)(0), Vprime.at(j % calUprime.size())));

    const string filename = "../test_data/simulatedSpace21Lattice.txt";
    write_points_to_ascii_file(filename, uv);
  }

  const int K2 = 2;
  size_t num_rep_y = 5;

  vector<Eigen::Matrix<double, K2, 1> > calUprime2 = generateUPrimeExample<K2>();
  vector<Eigen::Matrix<double, K2 ,1> > calU2  = getPointsinLattice<K2>(num_rep_x, num_rep_y);

  EXPECT(calU2.size() != 0)
  EXPECT(calUprime2.size() != 0)

  if(K2 == 2) {
    vector<Point2> uv;
    for(size_t j =0; j < calU2.size(); j++)
      uv.push_back(Point2(calU2.at(j)(0), calU2.at(j)(1)));
    const string filename = "../test_data/simulatedSpace22Lattice.txt";
    write_points_to_ascii_file(filename, uv);
  }

}

//******************************************************************************
TEST(getPoints , points) {
  const int K = 1;
  Vector1 b1; b1 << 0.5;
  Lattice<K> lattice(b1);
  UnitCell2 unitCell(Rot2(M_PI/ 4.0), Point2(1.0, 1.0));
  size_t num_rep_x = 5;
  vector<Point2> points = getPoints<K>(lattice, unitCell, num_rep_x);
  EXPECT(points.size() != 0)
  const string filename = "../test_data/simulatedSpace21.txt";
  write_points_to_ascii_file(filename, points);
}

//******************************************************************************
TEST(getPoints , point2d) {
  const int K = 2;
  Vector2 b1, b2; b1 << 1.0, 0.0; b2 << 0.0, 1.0;
  Lattice<K> lattice(b1, b2);
  UnitCell2 unitCell(Rot2(M_PI/ 4.0), Point2(1.0, 1.0));
  size_t num_rep_x = 5;
  size_t num_rep_y = 5;

  vector<Point2> points = getPoints<K>(lattice, unitCell, num_rep_x, num_rep_y);
  EXPECT(points.size() != 0)
  const string filename = "../test_data/simulatedSpace22.txt";
  write_points_to_ascii_file(filename, points);
}

TEST(getPoints , estimate1d) {
  const int K = 1;
  Vector1 b1; b1 << 0.5;
  Lattice<K> lattice(b1);
  UnitCell2 unitCell(Rot2(M_PI/ 4.0), Point2(1.0, 1.0));
  size_t num_rep_x = 5;

  vector<Point2> calZ = getPoints<K>(lattice, unitCell, num_rep_x);
  //  const string filename = "../test_data/simulatedSpace21.txt";
  //  write_points_to_ascii_file(filename, calZ);

  vector<Eigen::Matrix<double, K ,1> > calUprime = generateUPrimeExample<K>(); printf("Number of Points in Unit Cell = %lu\n", calUprime.size());

  vector<MillerIndices<K> > Q = generateMillerIndices<K>(calUprime.size(), num_rep_x); printf("Number of Miller indices = %lu\n", Q.size());

  vector<size_t> J = getPrimaryPointIndices(calUprime.size(), Q.size());

  vector<double> calVprime = getVprime<K>(calUprime, lattice);

  Estimate1DLattice2DSymmetry estimate(calZ, J, Q, calUprime, calVprime);
  estimate.setDebug(true);
  estimate.SetLatticeAndUnitCell(lattice, unitCell);
  estimate.optimizeBT();

  boost::shared_ptr<Lattice1> actual_lattice(new Lattice1());
  boost::shared_ptr<UnitCell2> actual_unitcell(new UnitCell2());
  estimate.getBT(actual_lattice, actual_unitcell);
  //  actual_lattice->print("Actual Lattice \n");
  //  actual_unitcell->print("Actual Unit Cell \n");
  printf("\n/* ************************************************************************* */\n");
}

TEST(getPoints , estimate1dpoints) {
  const int K = 1;
  Vector1 b1; b1 << 0.5;
  Lattice<K> lattice(b1);
//  UnitCell2 unitCell(Rot2(M_PI/4.0), Point2(1.0, 1.0));
    UnitCell2 unitCell(Rot2(0.0), Point2(1.0, 1.0));
  size_t num_rep_x = 2;

  vector<Point2> calZ = getPoints<K>(lattice, unitCell, num_rep_x);
  const string filename = "../test_data/simulatedSpace21.txt";
  write_points_to_ascii_file(filename, calZ);

  vector<Eigen::Matrix<double, K ,1> > calUprime = generateUPrimeExample<K>(); printf("Number of Points in Unit Cell = %lu\n", calUprime.size());

  vector<MillerIndices<K> > Q = generateMillerIndices<K>(calUprime.size(), num_rep_x); printf("Number of Miller indices = %lu\n", Q.size());

  vector<size_t> J = getPrimaryPointIndices(calUprime.size(), Q.size());

  vector<double> calVprime = getVprime<K>(calUprime, lattice);

  Estimate1DLattice2DSymmetry estimate(calZ, J, Q, calUprime, calVprime);
  estimate.SetLatticeAndUnitCell(lattice, unitCell);
  estimate.optimizeBTandPoints();

  boost::shared_ptr<Lattice1> actual_lattice(new Lattice1());
  boost::shared_ptr<UnitCell2> actual_unitcell(new UnitCell2());
  estimate.getBT(actual_lattice, actual_unitcell);
  actual_lattice->print("Actual Lattice \n");
  actual_unitcell->print("Actual Unit Cell \n");

}

TEST(getPoints , estimate2dpoints) {
  const int K = 2;
  Vector2 b1, b2;
  b1 << 1.0, 0.0;
  b2 << 0.0, 1.0;
  Lattice<K> lattice(b1, b2);
  UnitCell2 unitCell(Rot2(M_PI / 4.0), Point2(1.0, 1.0));
  size_t num_rep_x = 2;
  size_t num_rep_y = 2;

  vector < Point2 > calZ = getPoints<K>(lattice, unitCell, num_rep_x,
      num_rep_y);
  const string filename = "../test_data/simulatedSpace22.txt";
  write_points_to_ascii_file(filename, calZ);

  vector<Vector2> calUprime = generateUPrimeExample<K>();
  LONGS_EQUAL(4, calUprime.size());

  vector<MillerIndices<K> > Q = generateMillerIndices<K>(calUprime.size(),
      num_rep_x, num_rep_y);
  LONGS_EQUAL(16, Q.size());

  vector < size_t > J = getPrimaryPointIndices(calUprime.size(), Q.size());

  Estimate2DLattice2DSymmetry estimate(calZ, J, Q, calUprime); // TODO not give Uprime

  Symbol keyL('l', 0), keyU('x', 0);

  // Create ground truth values
  Values groundTruth;
  groundTruth.insert(keyL, lattice);
  groundTruth.insert(keyU, unitCell);

  // Check that error is zero at ground truth
  auto graph = estimate.createGraph(0.01, OptimizeFlag::spaceonly); // give UPrime
  EXPECT_DOUBLES_EQUAL(0, graph.error(groundTruth), 1e-9);

  // Check Hessian is full rank at ground truth
  auto gfg = graph.linearize(groundTruth);
  auto hessian_vector = gfg->hessian();
  Eigen::FullPivLU<Matrix> lu_decomp(hessian_vector.first);
  LONGS_EQUAL(6, lu_decomp.rank());

  // Create default initial estimate
  Values initial = estimate.initialEstimate(OptimizeFlag::spaceonly);

  // Check Hessian is full rank at default initial estimate
  auto gfg2 = graph.linearize(initial);
  auto hessian_vector2 = gfg2->hessian();
  Eigen::FullPivLU<Matrix> lu_decomp2(hessian_vector2.first);
  LONGS_EQUAL(6, lu_decomp2.rank());

  // Test optimization
  Values result = estimate.optimizeBT();

  // Check that error is zero at result
  EXPECT_DOUBLES_EQUAL(0, graph.error(result), 1e-9);

  // Check that result is equal to ground truth
  Lattice2 actual_lattice = result.at<Lattice2>(keyL);
  UnitCell2 actual_unitcell = result.at<UnitCell2>(keyU);
  EXPECT(assert_equal(lattice, actual_lattice));
  EXPECT(assert_equal(unitCell, actual_unitcell));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

