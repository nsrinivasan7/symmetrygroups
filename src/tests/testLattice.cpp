/*
 * testLattice.cpp
 *
 *  Created on: Apr 20, 2017
 *      Author: nsrinivasan7
 */
#include "../Lattice.h"

#include <gtsam/geometry/Pose3.h>
#include <CppUnitLite/TestHarness.h>

using namespace std;
using namespace gtsam;

//******************************************************************************
TEST(Lattice , Concept) {
  BOOST_CONCEPT_ASSERT((internal::HasVectorSpacePrereqs<Lattice2>));
//  BOOST_CONCEPT_ASSERT((IsGroup<Lattice2> ));
//  BOOST_CONCEPT_ASSERT((IsManifold<Lattice2> ));
//  BOOST_CONCEPT_ASSERT((IsVectorSpace<Lattice2> ));
  BOOST_CONCEPT_ASSERT((IsTestable<Lattice2> ));
}

//******************************************************************************
TEST(Lattice , dimension) {
  LONGS_EQUAL(1, Lattice1::dimension);
  LONGS_EQUAL(3, Lattice2::dimension);
  LONGS_EQUAL(6, Lattice3::dimension);
}

//******************************************************************************
TEST(Lattice , BasisOfVector) {
  Lattice2::VectorizedBasis v(1, 2, 3);
  Lattice2::Basis expected; expected << 1.0, 2.0, 0.0, 3.0;
  Lattice2 lattice(expected);
  EQUALITY(expected, lattice.BasisOfVector(v));
}

//******************************************************************************
namespace testCase {
Matrix2 B = (Matrix(2, 2) << 2, 1, 0, 1).finished();
}

//******************************************************************************
TEST(Lattice , Constructor) {
  using namespace testCase;
  Lattice2 lattice(B);
  EQUALITY(B, lattice.basis());
}

//******************************************************************************
TEST(Lattice , Constructor2) {
  using namespace testCase;
  Vector2 b1, b2;
  b1 << 2, 0;
  b2 << 1, 1;
  Lattice2 lattice(b1, b2);
  EQUALITY(B, lattice.basis());
}

//******************************************************************************
TEST(Lattice , Operators) {
  using namespace testCase;
  Lattice2 lattice(B);
  Lattice2 actual = lattice + lattice;
  Lattice2 expected(B * 2);
  CHECK(assert_equal<Lattice2>(expected, actual));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

