/*
 * testSpace.cpp
 *
 *  Created on: Apr 23, 2017
 *      Author: nsrinivasan7
 */

#include <CppUnitLite/TestHarness.h>
#include <gtsam/geometry/Pose3.h>
#include "../Space2.h"

using namespace std;
using namespace gtsam;

#define ANGLE M_PI/3.0
#define COMPANGLE 90.0 - M_PI/3.0

// ******************************************************************************

TEST(Space1 , Concept) {
//  BOOST_CONCEPT_ASSERT((internal::HasVectorSpacePrereqs<Space21>));
//  BOOST_CONCEPT_ASSERT((internal::HasVectorSpacePrereqs<Space22>));

//  BOOST_CONCEPT_ASSERT((IsGroup<Lattice2> ));
  BOOST_CONCEPT_ASSERT((IsManifold<Lattice2> ));
//  BOOST_CONCEPT_ASSERT((IsVectorSpace<Lattice2> ));

}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
