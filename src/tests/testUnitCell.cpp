#include "../UnitCell.h"

#include <CppUnitLite/TestHarness.h>

using namespace std;

//******************************************************************************
TEST(UnitCell3, Constructor) {
  gtsam::Pose3 p(gtsam::Rot3(), gtsam::Point3(1.0, 0.0, 0.0));
  UnitCell3 unitcell(p);
  EXPECT(assert_equal(p, unitcell));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
