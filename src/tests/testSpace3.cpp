/*
 * testSpace.cpp
 *
 *  Created on: Apr 23, 2017
 *      Author: nsrinivasan7
 */

#include <CppUnitLite/TestHarness.h>
#include <gtsam/geometry/Pose3.h>
#include "../Space3.h"

using namespace std;
using namespace gtsam;

//******************************************************************************
TEST(Space3 , lattice) {

  const int k = 2;

  Vector2 b1, b2;
  b1 << 2, 0;
  b2 << 1, 1;

  Matrix2 expected;
  expected.block<2, 1>(0, 0) = b1;
  expected.block<2, 1>(0, 1) = b2;

  Lattice<k> lattice(expected);
  Space3<k> space(lattice, Pose3());

  EQUALITY(expected, space.lattice().basis());

}

//******************************************************************************
TEST(Space3 , Bu) {

  const int k = 2;

  Vector2 b1, b2, u, expected;
  b1 << 2, 0;
  b2 << 1, 1;

  Matrix2 basis;
  basis.block<2, 1>(0, 0) = b1;
  basis.block<2, 1>(0, 1) = b2;

  Lattice<k> lattice(basis);
  Space3<k> space(lattice, Pose3());

  u << 1, 0;
  expected << 2, 0;
  EQUALITY(expected, space.getBu(u));

}

// ******************************************************************************

TEST(Space3 , defneR) {

  // define R
  // r1 = cos(M_PI/3) sin(M_PI/3 0
  // r2 = -sin(M_PI/3) cos(M_PI/3 0
  // r3 = 0 0 1

  Rot3 R(cos(M_PI / 3), -sin(M_PI / 3), 0, sin(M_PI / 3), cos(M_PI / 3), 0, 0,
      0, 1);

  // yaw =0, pith = 0, roll = 0 (rotate counter clockwise around z axis
  Rot3 expected = Rot3::Ypr(M_PI / 3,0, 0 );
  EQUALITY(expected.matrix(), R.matrix());

}

// ******************************************************************************
// We will first consider the case of a point on the plane, z = 0
//	Let us say that a point on the plane

TEST(Space3 , zeroz) {

  const int k = 2;
  const int n = 3;

  Vector2 b1, b2, u;
  Vector3 expected;
  b1 << 2, 1;
  b2 << 0, 1;

  Matrix2 basis;
  basis.block<2, 1>(0, 0) = b1;
  basis.block<2, 1>(0, 1) = b2;

  Lattice<k> lattice(basis);

  u << 1, 0;

  // define pose of unit cell
  // r1 = cos(M_PI/3) sin(M_PI/3 0
  // r2 = -sin(M_PI/3) cos(M_PI/3 0
  // r3 = 0 0 1
  Rot3 Rx(1, 0, 0, 0, cos(M_PI / 2), -sin(M_PI_2), 0, sin(M_PI / 2),
      cos(M_PI / 2));
  Rot3 Rt(cos(M_PI / 3), -sin(M_PI / 3), 0, sin(M_PI / 3), cos(M_PI / 3), 0, 0,
      0, 1);

  Rot3 R = Rt * Rx;

  Point3 t(0, 3, 2);
  Pose3 pose(R, t);
  Space3<k> space(lattice, pose);

  Vector2 y = space.getBu(u);

  // consider the a point on the x-z plane that is rotated by 60 wrt to the z-axis
  expected << y(0) * cos(M_PI / 3) + t(0), y(0) * sin(M_PI / 3) + t(1), y(1)
          + t(2);

  Eigen::Matrix<double, n - k, 1> z;
  z << 0;
  Eigen::Matrix<double, n, 1> actual = space.getx(u, z);

  EXPECT(assert_equal(expected, actual));
}

// ******************************************************************************

TEST(Space3 , nonzeroz) {

  const int k = 2;
  const int n = 3;

  Vector2 b1, b2, u;
  Vector3 expected;
  b1 << 2, 0;
  b2 << 1, 1;

  Matrix2 basis;
  basis.block<2, 1>(0, 0) = b1;
  basis.block<2, 1>(0, 1) = b2;

  Lattice<k> lattice(basis);

  u << 1, 0;

  // define pose of unit cell
  // r1 = cos(M_PI/3) sin(M_PI/3 0
  // r2 = -sin(M_PI/3) cos(M_PI/3 0
  // r3 = 0 0 1
  Rot3 Rx(1, 0, 0, 0, cos(M_PI / 2), -sin(M_PI_2), 0, sin(M_PI / 2),
      cos(M_PI / 2));
  Rot3 Rt(cos(M_PI / 3), -sin(M_PI / 3), 0, sin(M_PI / 3), cos(M_PI / 3), 0, 0,
      0, 1);

  Rot3 R = Rt * Rx;

  Point3 t(0, 3, 2);
  Pose3 pose(R, t);
  Space3<k> space(lattice, pose);

  Vector2 y = space.getBu(u);

  // specify a non-zero z
  Eigen::Matrix<double, n - k, 1> z;
  z << 0.2;

  // consider the a point on the x-z plane that is rotated by 60 wrt to the z-axis
  expected << (y(0) * cos(M_PI / 3) + z(0) * sin(M_PI / 3) + t(0)), (y(0)
      * sin(M_PI / 3) - z(0) * cos(M_PI / 3) + t(1)), (y(1) + t(2));

  Eigen::Matrix<double, n, 1> actual = space.getx(u, z);

  EXPECT(assert_equal(expected, actual));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
