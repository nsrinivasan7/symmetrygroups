/*
 * testSpace.cpp
 *
 *  Created on: Apr 23, 2017
 *      Author: nsrinivasan7
 */
#include <CppUnitLite/TestHarness.h>
#include <gtsam/geometry/Pose3.h>

#include "../Space1.h"


using namespace std;
using namespace gtsam;

typedef Space1<1> Space11;

// ******************************************************************************

TEST(Space1 , Concept) {
  BOOST_CONCEPT_ASSERT((internal::HasVectorSpacePrereqs<Space11>));
//  BOOST_CONCEPT_ASSERT((IsGroup<Lattice2> ));
//  BOOST_CONCEPT_ASSERT((IsManifold<Lattice2> ));
//  BOOST_CONCEPT_ASSERT((IsVectorSpace<Lattice2> ));
  BOOST_CONCEPT_ASSERT((IsTestable<Lattice2> ));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
