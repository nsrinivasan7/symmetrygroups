/*
 * testPredict.cpp
 *
 *  Created on: Apr 25, 2017
 *      Author: nsrinivasan7
 */

#include "../Lattice.h"
#include "../MillerIndices.h"
#include "../Predict.h"

#include <CppUnitLite/TestHarness.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/base/numericalDerivative.h>

using namespace std;
using namespace gtsam;
//******************************************************************************
// test that the point is being translated correctly
// first check the 1D case

typedef Eigen::Matrix<double, 1, 1> Point1n;
typedef Eigen::Matrix<double, 2, 1> Point2n;
typedef Eigen::Matrix<double, 3, 1> Point3n;

/* ************************************************************************* */
// derivative of a 1D lattice in \R_{1}
TEST(Predict , derivative1) {

  //specify the space dimension
  const int k = 1;

  //define a Miller index
  const int q = 3;
  const MillerIndices<k> millerIndex(q);
  PredictPointInLattice<k> predict(millerIndex);

  // specify first basis, which in 1D is just lattice scale
  double latticeScale = 0.4;
  Point1n b1;
  b1 << latticeScale;
  Lattice<k> lattice(b1);

  Point1n uprime;
  uprime << 0.5;

  // create function f(uprime) = B*(uprime + q)
  boost::function<double(const double&, const double&)> f =
      [](const double& uprime_0, const double& latticeBasis) {
    Point1n uprime;
    uprime << uprime_0;
    Point1n b1;
    b1 << latticeBasis;
    Lattice<k> lattice(b1);
    MillerIndices<k> millerIndex(q);
    PredictPointInLattice<k> predict(millerIndex);
    Point1n result = predict(uprime, lattice);
    return result(0);
  };

  Matrix H1, H2;
  // predict a point in lattice
  Point1n expected;
  expected << (q + uprime(0)) * latticeScale;
  Point1n actual = predict(uprime, lattice, H1, H2);
  EQUALITY(expected, actual);

  Matrix expectedH1 = numericalDerivative21<double, double>(f, uprime(0),
      lattice.basis()(0, 0));
  Matrix expectedH2 = numericalDerivative22<double, double>(f, uprime(0),
      lattice.basis()(0, 0));

  EXPECT(assert_equal(expectedH1, H1, 1e-9));
  EXPECT(assert_equal(expectedH2, H2, 1e-9));

}

/* ************************************************************************* */

// derivative of a 2D lattice in \R_{2}
TEST(Predict , derivative2) {
  //specify the space dimension
  const int k = 2;

  //define a miller index
  const int q1 = 3;
  const int q2 = 2;
  MillerIndices<k> millerindex(q1, q2);
  PredictPointInLattice<k> predict(millerindex);

  // specify lattice dimension
  Lattice<k>::BasisVectors b1, b2;
  b1 << 1, 0;
  b2 << 0.5, 0.5;
  Lattice<k> lattice(b1, b2);

  //define coordinated inside the unit cell
  Point2n uprime;
  uprime << 0.5, 0.4;

  // create function f(uprime) = B*(uprime + q)
  // lattice is Point3 here becasue 2D lattice has only 3 free parameters
  boost::function<Point2(const Point2&, const Point3&)> f =
      [](const Point2& uprime_val, const Point3& latticeParameters) {
    Matrix22 B; B << latticeParameters.x(), latticeParameters.y(), 0, latticeParameters.z();
    Lattice<k> lattice(B);
    Point2n uprime;
    uprime << uprime_val(0), uprime_val(1) ;
    MillerIndices<k> millerIndex(q1, q2);
    PredictPointInLattice<k> predict(millerIndex);
    Point2n result = predict(uprime, lattice);
    return Point2(result(0), result(1));
  };

  Matrix H1, H2;
  // predict a point in lattice
  Point2n actual = predict(uprime, lattice, H1, H2);

  Matrix22 B;
  B << 1, 0.5, 0, 0.5;
  Point2n q;
  q << q1, q2;
  Point2n expected;
  expected << B * (q + uprime);

  EQUALITY(expected, actual);

  Matrix expectedH1 = numericalDerivative21(f, Point2(uprime(0), uprime(1)),
      Point3(b1(0), b2(0), b2(1)));
  Matrix expectedH2 = numericalDerivative22(f, Point2(uprime(0), uprime(1)),
      Point3(b1(0), b2(0), b2(1)));

  EXPECT(assert_equal(expectedH1, H1, 1e-9));
  EXPECT(assert_equal(expectedH2, H2, 1e-9));

}

/* ************************************************************************* */

// test "transform_from" after applying Exmap and logmap
Point3 transform_from_(const Point3& point, const Vector6& poseVector ) {
  Pose3 pose = Pose3::Expmap(poseVector);
  return pose.transform_from(point);
}

TEST( Pose3, Dtransform_from1)
{
  Pose3 T(Rot3(), Point3(0.0,0.0,10.0));
  Vector6 poseVector = Pose3::Logmap(T);
  Point3 p(1.2, 4.7, 0.0);
  Matrix actualDtransform_from;
  T.transform_from(p, actualDtransform_from, boost::none);
  Matrix actual_expmap;
  T = Pose3::Expmap(poseVector, actual_expmap);
  Matrix numerical = numericalDerivative22(transform_from_,p, poseVector);
  Matrix expected = actualDtransform_from*actual_expmap;
  EXPECT(assert_equal(numerical,expected,1e-5));
}

/* ************************************************************************* */
// derivative of a 2D lattice in \R_{3}
//TEST(Predict , derivative3) {
//  //specify the space dimension
//  const int k = 2;
//  const int n = 3;
//
//  //define a miller index
//  const int q1 = 3;
//  const int q2 = 2;
//  MillerIndices<k> millerindex(q1, q2);
//
//  // specify lattice dimension
//  Lattice<k>::BasisVectors b1, b2;
//  b1 << 1, 0;
//  b2 << 0.5, 0.5;
//
//  const double translation_z = 10.0;
//  UnitCell3 unitcell(gtsam::Rot3(),gtsam::Point3(0,0,translation_z));
//  Matrix exmap_derivative;
//  Vector6 lm = gtsam::Pose3::Logmap(unitcell);
//  unitcell = Pose3::Expmap(lm, exmap_derivative);
//
//  Space3<k> space(Lattice<k>(b1,b2), unitcell);
//
//  const int z_value = 0.0;
//  Eigen::Matrix<double,n-k,1> z;
//  z << z_value;
//  PredictPointInSpace3<k> predict(millerindex, z);
//
//
//  //define coordinated inside the unit cell
//  Point2n uprime;
//  uprime << 0.5, 0.4;
//
//  // create function f(uprime) = [B*(uprime + q); z] *
//  boost::function<Point3(const Point2&, const Vector9&)> f =
//      [](const Point2& uprime_val, const Vector9& latticeParameters) {
//    Matrix22 B; B << latticeParameters(0), latticeParameters(1), 0, latticeParameters(2);
//    Lattice<k> lattice(B);
//    Eigen::Matrix<double,n-k,1> z;
//    z << z_value;
//    Vector6 exp_pose3;
//    exp_pose3 << latticeParameters.block(3,0,6,1);
//    Matrix exmap_H1;
//    UnitCell3 unitcell(gtsam::Pose3::Expmap(exp_pose3, exmap_H1));
//    Space3<k> space(lattice, unitcell);
//    Point2n uprime;
//    uprime << uprime_val(0), uprime_val(1);
//    MillerIndices<k> millerindex(q1, q2);
//    PredictPointInSpace3<k> predict(millerindex, z);
//    return  predict(uprime, space);
//  };
//
//    Matrix H1, H2;
//    // predict a point in lattice
//    Point3 actual = predict(uprime, space, H1, H2);
//
//    Matrix22 B;
//    B << 1, 0.5, 0, 0.5;
//    Point2n q;
//    q << q1, q2;
//
//    Point3n augmented_y;
//    augmented_y.block<2,1>(0,0) << B * (q + uprime);
//    augmented_y(2) = z_value;
//
//    Point3 translation(0.0, 0.0, translation_z);
//    Point3 expected = Point3(augmented_y(0), augmented_y(1), augmented_y(2)) + translation;
//
//    EQUALITY(expected, actual);
//    Vector9 val; val <<  b1(0), b2(0), b2(1), lm(0), lm(1), lm(2), lm(3), lm(4), lm(5);
//    Matrix expectedH1 = numericalDerivative21(f, Point2(uprime(0), uprime(1)), val);
//    Matrix expectedH2 = numericalDerivative22(f, Point2(uprime(0), uprime(1)), val);
//
//    Matrix actual_H2 = H2;
//    actual_H2.block(0, k*(k+1)/2, 3, 6) << H2.block(0, k*(k+1)/2, 3, 6) * exmap_derivative;
//    EXPECT(assert_equal(expectedH1, H1, 1e-9));
//    EXPECT(assert_equal(expectedH2, actual_H2, 1e-5));
//
//}


/* ************************************************************************* */
// derivative of a 2D lattice in \R_{3} by using a 3 elements function for space
//TEST(Predict , derivative4) {
//  //specify the space dimension
//  const int k = 2;
//  const int n = 3;
//
//  //define a miller index
//  const int q1 = 3;
//  const int q2 = 2;
//  MillerIndices<k> millerindex(q1, q2);
//
//  // specify lattice dimension
//  Lattice<k>::BasisVectors b1, b2;
//  b1 << 1, 0;
//  b2 << 0.5, 0.5;
//
//  const double translation_z = 10.0;
//  UnitCell3 unitcell(gtsam::Rot3(),gtsam::Point3(0,0,translation_z));
//
//  Space3<k> space(Lattice<k>(b1,b2), unitcell);
//
//  const int z_value = 0.0;
//  Eigen::Matrix<double,n-k,1> z;
//  z << z_value;
//  PredictPointInSpace3<k> predict(millerindex, z);
//
//
//  //define coordinated inside the unit cell
//  Point2n uprime;
//  uprime << 0.5, 0.4;
//
//  // create function f(uprime) = [B*(uprime + q); z] *
//  boost::function<Point3(const Point2&, const Point3&, const Pose3&)> f =
//      [](const Point2& uprime_val, const Point3& latticeParameters, const Pose3& pose) {
//    Matrix22 B; B << latticeParameters.x(), latticeParameters.y(), 0, latticeParameters.z();
//    Lattice<k> lattice(B);
//    Eigen::Matrix<double,n-k,1> z;
//    z << z_value;
//    UnitCell3 unitcell(pose);
//    Space3<k> space(lattice, unitcell);
//    Point2n uprime;
//    uprime << uprime_val(0), uprime_val(1);
//    MillerIndices<k> millerindex(q1, q2);
//    PredictPointInSpace3<k> predict(millerindex, z);
//    return  predict(uprime, space);
//  };
//
//    Matrix H1, H2;
//    // predict a point in lattice
//    Point3 actual = predict(uprime, space, H1, H2);
//
//    Matrix22 B;
//    B << 1, 0.5, 0, 0.5;
//    Point2n q;
//    q << q1, q2;
//
//    Point3n augmented_y;
//    augmented_y.block<2,1>(0,0) << B * (q + uprime);
//    augmented_y(2) = z_value;
//
//    Point3 translation(0.0, 0.0, translation_z);
//    Point3 expected = Point3(augmented_y(0), augmented_y(1), augmented_y(2)) + translation;
//
//    EQUALITY(expected, actual);
//    Point3 latticeparams(b1(0), b2(0), b2(1));
//    Matrix expectedH1 = numericalDerivative31(f, Point2(uprime(0), uprime(1)), latticeparams, unitcell);
//    Matrix expectedH2 = numericalDerivative32(f, Point2(uprime(0), uprime(1)), latticeparams, unitcell);
//    Matrix expectedH3 = numericalDerivative33(f, Point2(uprime(0), uprime(1)), latticeparams, unitcell);
//
//    EXPECT(assert_equal(expectedH1, H1, 1e-9));
//    EXPECT(assert_equal(expectedH2, H2.block(0,0,3,3), 1e-9));
//    EXPECT(assert_equal(expectedH3, H2.block(0,3,3,6), 1e-9));
//}

/* ************************************************************************* */
// derivative of a 1D lattice in \R_23} by using a 3 elements function for space
TEST(Predict , derivative5) {
  //specify the space dimension
  const int k = 1;

  //define a miller index
  const int q1 = 2;
  MillerIndices<k> millerindex(q1);

  // specify lattice dimension
  Lattice<k>::BasisVectors b1;
  b1 << 1;

  const double translation_y = 10.0;
  gtsam::Rot2 rotation(M_PI/4.0);
  UnitCell2 unitcell(rotation,gtsam::Point2(0,translation_y));
  Lattice<k> lattice(b1);

  const int vprime = 0;
  PredictPointInSpace21 predict(millerindex, (double)vprime);


  //define coordinated inside the unit cell
  double uprime = 0.5;

  // create function f(uprime) = [B*(uprime + q); z] *
  boost::function<Point2(const double&, const double&, const Pose2&)> f =
      [](const double& uprime_val, const double& latticeParameters, const Pose2& pose) {
    Matrix11 B; B << latticeParameters;
    Lattice<k> lattice(B);
    UnitCell2 unitcell(pose);
    MillerIndices<k> millerindex(q1);
    PredictPointInSpace21 predict(millerindex, (double)vprime);
    return  predict(uprime_val, lattice, unitcell);
  };

  Matrix H1, H2, H3;
  // predict a point in lattice
  Point2 actual = predict(uprime, lattice, unitcell, H1, H2, H3);

  Matrix11 B;
  B << b1;
  Point1n q;
  q << q1;
  Vector1 uprime_vector; uprime_vector << uprime;
  Point2n augmented_y;
  augmented_y.block<1,1>(0,0) << B * (q + uprime_vector);
  augmented_y(1) = vprime;

  Point2 translation(0.0, translation_y);
  Point2 expected = Point2(rotation.matrix() * augmented_y) + translation;

  EQUALITY(expected, actual);
  double latticeparams = b1(0);
  Matrix expectedH1 = numericalDerivative31(f, uprime, latticeparams, unitcell);
  Matrix expectedH2 = numericalDerivative32(f, uprime, latticeparams, unitcell);
  Matrix expectedH3 = numericalDerivative33(f, uprime, latticeparams, unitcell);

  EXPECT(assert_equal(expectedH1, H1, 1e-9));
  EXPECT(assert_equal(expectedH2, H2, 1e-9));
  EXPECT(assert_equal(expectedH3, H3, 1e-9));
}

// derivative of a 1D lattice in \R_23} by using a 3 elements function for space
TEST(Predict , derivative51) {
  //specify the space dimension
  const int k = 1;

  //define a miller index
  const int q1 = 2;
  MillerIndices<k> millerindex(q1);

  // specify lattice dimension
  Lattice<k>::BasisVectors b1;
  b1 << 1;

  const double translation_y = 10.0;
  gtsam::Rot2 rotation(M_PI/4.0);
  UnitCell2 unitcell(rotation,gtsam::Point2(0,translation_y));
  Lattice<k> lattice(b1);

  const double vprime = 0;
  PredictPointInSpace21GivenUnitCell predict(millerindex);


  //define coordinated inside the unit cell
  double uprime = 0.5;

  // create function f(uprime) = [B*(uprime + q); z] *
  boost::function<Point2(const double&, const double&, const double&)> f =
      [](const double& uprime_val, const double& vprime_val, const double& latticeParameters) {
    Matrix11 B; B << latticeParameters;
    Lattice<k> lattice(B);
    MillerIndices<k> millerindex(q1);
    PredictPointInSpace21GivenUnitCell predict(millerindex);
    return  predict(uprime_val, vprime_val, lattice);
  };

  Matrix H1, H2, H3;
  // predict a point in lattice
  Point2 actual = predict(uprime, vprime, lattice, H1, H2, H3);

  Matrix11 B;
  B << b1;
  Point1n q;
  q << q1;
  Vector1 uprime_vector; uprime_vector << uprime;
  Point2n augmented_y;
  augmented_y.block<1,1>(0,0) << B * (q + uprime_vector);
  augmented_y(1) = vprime;

  Point2 expected = Point2(augmented_y(0), augmented_y(1));

  EQUALITY(expected, actual);
  double latticeparams = b1(0);
  Matrix expectedH1 = numericalDerivative31(f, uprime, vprime, latticeparams);
  Matrix expectedH2 = numericalDerivative32(f, uprime, vprime, latticeparams);
  Matrix expectedH3 = numericalDerivative33(f, uprime, vprime, latticeparams);

  EXPECT(assert_equal(expectedH1, H1, 1e-9));
  EXPECT(assert_equal(expectedH2, H2, 1e-9));
  EXPECT(assert_equal(expectedH3, H3, 1e-9));
}

/* ************************************************************************* */
// derivative of a 1D lattice in \R_23} by using a 3 elements function for space
TEST(Predict , chainrule) {
  //specify the space dimension
  const int k = 1;

  //define a miller index
  const int q1 = 2;
  MillerIndices<k> millerindex(q1);

  // specify lattice dimension
  Lattice<k>::BasisVectors b1;
  b1 << 1;

  const double translation_y = 10.0;
  gtsam::Rot2 rotation(M_PI/4.0);
  UnitCell2 unitcell(rotation,gtsam::Point2(0,translation_y));
  Lattice<k> lattice(b1);

  const int vprime = 1.0;
  PredictPointInSpace21 predict(millerindex, (double)vprime);

  PredictPointInSpace21GivenUnitCell predict2(millerindex);

  //define coordinated inside the unit cell
  double uprime = 0.5;

  // create function f(uprime) = [B*(uprime + q); z] *
  boost::function<Point2(const double&, const double&, const Pose2&)> f =
      [](const double& uprime_val, const double& latticeParameters, const Pose2& pose) {
    Matrix11 B; B << latticeParameters;
    Lattice<k> lattice(B);
    UnitCell2 unitcell(pose);
    MillerIndices<k> millerindex(q1);
    PredictPointInSpace21 predict(millerindex, (double)vprime);
    return  predict(uprime_val, lattice, unitcell);
  };

  Matrix H1, H2, H3;
  Matrix E_H1, E_H2, E_H3, E_H4, E_H5;
  // predict a point in lattice
  Point2 actual = predict(uprime, lattice, unitcell, H1, H2, H3);
  Point2 expected_temp = predict2(uprime, vprime, lattice, E_H1, E_H2, E_H3);
  transform_from_Pose2 tform;
  Point2 expected = tform(expected_temp, unitcell, E_H4, E_H5);

  EQUALITY(expected, actual);
  EXPECT(assert_equal(E_H5, H3, 1e-9));
  EXPECT(assert_equal(E_H4*E_H3, H2, 1e-9));
  EXPECT(assert_equal(E_H4*E_H1, H1, 1e-9));

}
/* ************************************************************************* */
// derivative of a 1D lattice in \R_1 by using a 3 elements function for space
TEST(Predict , derivative6) {
  //specify the space dimension
  const int k = 1;

  //define a miller index
  const int q1 = 2;
  MillerIndices<k> millerindex(q1);

  // specify lattice dimension
  Lattice<k>::BasisVectors b1;
  b1 << 1;

  const double translation_y = 10.0;
  UnitCell1 unitcell = translation_y;

  Space1<k> space(Lattice<k>(b1), unitcell);

  PredictPointInSpace1 predict(millerindex);


  //define coordinated inside the unit cell
  double uprime = 0.5;

  // create function f(uprime) = [B*(uprime + q); z] *
  boost::function<double(const double&, const double&, const double&)> f =
      [](const double& uprime, const double& latticeParameters, const double& pose) {
    Matrix11 B; B << latticeParameters;
    Lattice<k> lattice(B);
    UnitCell1 unitcell = pose;
    Space1<k> space(lattice, unitcell);
    MillerIndices<k> millerindex(q1);
    PredictPointInSpace1 predict(millerindex);
    return  predict(uprime, space);
  };

  Matrix H1, H2;
  // predict a point in lattice
  double actual = predict(uprime, space, H1, H2);

  Matrix11 B;
  B << b1;
  Point1n q;
  q << q1;

  double expected = (uprime + (double)q1) * b1(0) + translation_y;

  EQUALITY(expected, actual);

  double latticeparams = b1(0);
  Matrix expectedH1 = numericalDerivative31(f, uprime, latticeparams, unitcell);
  Matrix expectedH2 = numericalDerivative32(f, uprime, latticeparams, unitcell);
  Matrix expectedH3 = numericalDerivative33(f, uprime, latticeparams, unitcell);

  EXPECT(assert_equal(expectedH1, H1, 1e-9));
  EXPECT(assert_equal(expectedH2, H2.block(0,0,1,1), 1e-9));
  EXPECT(assert_equal(expectedH3, H2.block(0,1,1,1), 1e-9));
}

///* ************************************************************************* */
// derivative of a 1D lattice in \R_1 by using a 3 elements function for space
TEST(Predict , derivative7) {
  //specify the space dimension
  const int k = 1;

  //define a miller index
  const int q1 = 2;
  MillerIndices<k> millerindex(q1);

  // specify lattice dimension
  Lattice<k>::BasisVectors b1;
  b1 << 2.0;

  const double translation_y = 10.0;
  UnitCell1 unitcell = translation_y;
  Lattice<k> lattice(b1);
  Space1<k> space(lattice, unitcell);

  PredictPointInSpaceGivenUnitCell1 predict(millerindex, unitcell);


  //define coordinates inside the unit cell
  double uprime = 0.5;

  // create function f(uprime) = [B*(uprime + q); z] *
  boost::function<double(const double&, const double&, const double&)> f =
      [](const double& uprime, const double& latticeParameters, const double& pose) {
    Matrix11 B; B << latticeParameters;
    Lattice<k> lattice(B);
    UnitCell1 unitcell = pose;
    Space1<k> space(lattice, unitcell);
    MillerIndices<k> millerindex(q1);
    PredictPointInSpaceGivenUnitCell1 predict(millerindex,unitcell);
    return  predict(uprime, lattice);
  };

  Matrix H1, H2;
  // predict a point in lattice
  double actual = predict(uprime, lattice, H1, H2);

  Matrix11 B;
  B << b1;
  Point1n q;
  q << q1;

  double expected = (uprime + (double)q1) * b1(0) + translation_y;

  EQUALITY(expected, actual);

  double latticeparams = b1(0);
  Matrix expectedH1 = numericalDerivative31(f, uprime, latticeparams, unitcell);
  Matrix expectedH2 = numericalDerivative32(f, uprime, latticeparams, unitcell);

  EXPECT(assert_equal(expectedH1, H1, 1e-9));
  EXPECT(assert_equal(expectedH2, H2.block(0,0,1,1), 1e-9));
}

///* ************************************************************************* */
// derivative of a 1D lattice in \R_1 by using a 3 elements function for space
TEST(Predict , derivative8) {
  //specify the space dimension
  const int k = 2;

  //define a miller index
  const int q1 = 3;
  const int q2 = 2;
  MillerIndices<k> millerindex(q1, q2);

  // specify lattice dimension
  Lattice<k>::BasisVectors b1, b2;
  b1 << 1.0, 0.0;
  b2 << 0.5, 0.5;
  Lattice<k> lattice(b1, b2);

  //define coordinated inside the unit cell
  Point2 uprime(0.5, 0.4);
  UnitCell2 unitcell(Rot2(M_PI/ 4.0), Point2(1.0, 1.0));

  PredictPointInSpace22 predict(millerindex);


  // create function f(uprime) = [B*(uprime + q); z] *
  boost::function<Point2(const Point2&, const Point3&, const Pose2&)> f =
      [](const Point2& uprime, const Point3& latticeParameters, const Pose2& pose) {
    Matrix22 B; B << latticeParameters.x(), latticeParameters.y(), 0.0, latticeParameters.z();
    Lattice<k> lattice(B);
    UnitCell2 unitcell(pose);
    MillerIndices<k> millerindex(q1, q2);
    PredictPointInSpace22 predict(millerindex);
    return  predict(uprime, lattice, unitcell);
  };

  Matrix22 H1;
  Matrix23 H2;
  Matrix23 H3;
  // predict a point in lattice
  Point2 actual = predict(uprime, lattice, unitcell, H1, H2, H3);

  Point2 expected = unitcell.transform_from(Point2(lattice.basis() * (uprime.vector() + millerindex.indices().template cast<double>())));
  EQUALITY(expected, actual);

  Point3 latticeparams(b1(0), b2(0), b2(1));
  Matrix expectedH1 = numericalDerivative31(f, uprime, latticeparams, unitcell);
  Matrix expectedH2 = numericalDerivative32(f, uprime, latticeparams, unitcell);
  Matrix expectedH3 = numericalDerivative33(f, uprime, latticeparams, unitcell);

  EXPECT(assert_equal(expectedH1, H1, 1e-9));
  EXPECT(assert_equal(expectedH2, H2, 1e-9));
  EXPECT(assert_equal(expectedH3, H3, 1e-9));
}
/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
