/*
 * Space-inl.hpp
 *
 *  Created on: Apr 23, 2017
 *      Author: nsrinivasan7
 *      description : Contains a representation of points in 3D space that
 *      transforms from lattice coordinates to 3D coordinates
 */

#ifndef SPACE_INL_HPP_
#define SPACE_INL_HPP_

#include "Lattice.h"
#include "UnitCell.h"

#include <gtsam/geometry/Pose3.h>
#include <boost/optional.hpp>

using namespace gtsam;

// a K-dimensional lattice in a 3-dimensional space
template<int K>
class Space3 {
public:

  typedef Eigen::Matrix<double, K, 1> PointK;

private:

  Lattice<K> lattice_;
  UnitCell3 unitcell_;

public:
  Space3(const Lattice<K>& lattice, const UnitCell3& pose) :
      lattice_(lattice), unitcell_(pose) {
    assert(K > 3);
  }

  const Lattice<K>& lattice() const {
    return lattice_;
  }

  PointK getBu(const PointK& u) {
    return (lattice_.basis() * u);
  }

  Point3 getx(const PointK& u,
      boost::optional<const Eigen::Matrix<double, 3 - K, 1>&> z = boost::none) {
    if (z && 3 == K)
      assert("z has to be zero for this setting \n");

    Point3 yz;
    PointK y = getBu(u);

    if (z) {
      yz.block(0, 0, K, 1) = y;
      yz.block(K, 0, 3 - K, 1) = *z;
    } else {
      yz.block(0, 0, K, 1) = y;
    }

    return (unitcell_.rotation().matrix() * yz
        + unitcell_.translation());
  }

  Point3 getxfromy(const PointK& y,
      boost::optional<const Eigen::Matrix<double, 3 - K, 1>&> z = boost::none) {
    if (z && 3 == K)
      assert("z has to be zero for this setting \n");

    Point3 yz;

    if (z) {
      yz.block(0, 0, K, 1) = y;
      yz.block(K, 0, 3 - K, 1) = *z;
    } else {
      yz.block(0, 0, K, 1) = y;
    }

    return (unitcell_.rotation().matrix() * yz
        + unitcell_.translation());
  }

  const UnitCell3& unitcell() const {
    return unitcell_;
  }
};

#endif /* SPACE_INL_HPP_ */
