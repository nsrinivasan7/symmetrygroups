import matplotlib.pyplot as plt
import numpy as np


def visualize_points(filename, dimensions=2):
    A = np.loadtxt(filename)
    if dimensions == 1:
        plt.scatter(A, np.zeros(len(A)),  facecolors='none', edgecolors='#5B80D5')
        plt.gca().set_aspect('equal', adjustable='box')
        x1, x2, y1, y2 = plt.axis()
        plt.axis((x1, x2, -3, 3))
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('2D Point in a Lattice(Scatter)')
        plt.grid(True)
        plt.savefig("test.png")
        plt.show()

    if dimensions == 2:
        plt.scatter(A[:, 0], A[:, 1], facecolors='none', edgecolors='#5B80D5')
        plt.gca().set_aspect('equal', adjustable='box')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('2D Point in a Lattice(Scatter)')
        plt.grid(True)
        plt.savefig("test.png")
        plt.show()
