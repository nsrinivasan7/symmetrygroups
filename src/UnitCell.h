/*
 * UnitCell.h
 *
 *  Created on: Apr 20, 2017
 *      Author: nsrinivasan7
 */

#ifndef SRC_UNITCELL_H_
#define SRC_UNITCELL_H_

#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Pose2.h>

namespace gtsam {


typedef gtsam::Pose3 UnitCell3;
typedef gtsam::Pose2 UnitCell2;
typedef double UnitCell1;

}

#endif /* SRC_UNITCELL_H_ */
