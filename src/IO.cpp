/*
 * IO.cpp
 *
 *  Created on: Sep 30, 2017
 *      Author: nsrinivasan7
 */

#include <boost/filesystem.hpp>

#include "IO.h"

namespace fs = boost::filesystem;

void create_full_path_to_test_data(const string filename) {
  cout << "Current path = " << fs::current_path() << endl;
}

void write_points_to_ascii_file(const string filename, const vector<Point2>& points) {

  fs::path filename_p(filename);
  if(fs::exists(filename_p)) {
    fs::remove(filename_p);
  }

  if(!fs::exists(filename_p.parent_path()))
    fs::create_directory(filename_p.parent_path());

  ofstream ofs(filename, ios::app);
  for(size_t j =0; j < points.size(); j++)
    ofs << points[j].x() << " " << points[j].y() << "\n";

  ofs.close();
}

void write_values_to_ascii_file(const string filename, const vector<double>& values) {

  fs::path filename_p(filename);
  if(fs::exists(filename_p)) {
    fs::remove(filename_p);
  }

  if(!fs::exists(filename_p.parent_path()))
    fs::create_directory(filename_p.parent_path());

  ofstream ofs(filename, ios::app);
  for(size_t j =0; j < values.size(); j++)
    ofs << values[j] << "\n";

  ofs.close();
}
